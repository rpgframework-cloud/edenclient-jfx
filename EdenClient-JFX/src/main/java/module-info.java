module de.rpgframework.eden.client.jfx {
	exports de.rpgframework.eden.client.jfx;
	exports de.rpgframework.eden.client.jfx.steps;
	opens de.rpgframework.eden.client.jfx;

	requires com.gluonhq.attach.browser;
	requires transitive com.gluonhq.attach.settings;
	requires com.gluonhq.attach.util;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.rules;
	requires transitive de.rpgframework.eden.client;
	requires java.prefs;
	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.extensions;
	requires javafx.graphics;
	requires com.gluonhq.attach.storage;
	requires de.rpgframework.javafx;
	requires org.controlsfx.controls;
	requires com.gluonhq.attach.device;
	requires com.gluonhq.attach.display;
	requires itextpdf;
	requires rpgframework.pdfviewer;

}