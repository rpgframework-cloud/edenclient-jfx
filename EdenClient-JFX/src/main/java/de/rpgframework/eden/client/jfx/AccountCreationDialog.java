package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.BiConsumer;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDualDialog;
import org.prelle.javafx.TitledComponent;

import com.gluonhq.attach.browser.BrowserService;

import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenConnection;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class AccountCreationDialog extends ManagedDualDialog {

	private final static Logger logger = System.getLogger("eden.client.jfx");

	public final static ResourceBundle RES = ResourceBundle.getBundle(LoginDialog.class.getPackageName()+".Dialogs");

	private BiConsumer<String, String> credentialsWriter;

	private Label lbIntro;
	private Hyperlink lbLink;
	private TextField tfUser;
	private TextField tfEmail;
	private TextField tfFirst, tfLast;
	private PasswordField tfPass;
	private ChoiceBox<Locale> cbLoc;

	private TextField tfUser2;
	private PasswordField tfPass2;

	private Label lbWarn;
	private Label lbWarn2;
	private Button btnRecover;
	private EdenAccountInfo ret;
	private EdenConnection con;

	//-------------------------------------------------------------------
	public AccountCreationDialog(EdenConnection con, BiConsumer<String, String> credentialsWriter) {
		super(ResourceI18N.get(RES, "account.title"), null, CloseType.YES, CloseType.NO);
		this.credentialsWriter = credentialsWriter;
		this.con = con;
		setTitle2(ResourceI18N.get(RES, "account.title2"));
		initComponents();
		initComponents2();
		initLayout();
		initLayout2();
		getButtons2().addAll(CloseType.CANCEL, CloseType.APPLY, CloseType.RECOVER);
		initInteractivity();
		buttonDisabledProperty().put(CloseType.YES, true);
		buttonDisabledProperty().put(CloseType.APPLY, true);
		buttonDisabledProperty().put(CloseType.RECOVER, true);

		super.setOnAction(CloseType.YES, ev -> {
			logger.log(Level.INFO, "create account on server");
			try {
				ret = con.createAccount(tfUser.getText(), tfEmail.getText(), tfPass.getText(), tfFirst.getText(), tfLast.getText(), cbLoc.getValue());
				logger.log(Level.INFO, "Server confirmed");
				lbWarn.setText(null);
				lbWarn.setVisible(false);
				credentialsWriter.accept(tfUser.getText(), tfPass.getText());
				getScreenManager().close(this, CloseType.YES);
			} catch (EdenAPIException e) {
				logger.log(Level.INFO, "Server error response: "+e.getCode()+" "+e.getLocalizedMessage());
				lbWarn.setText(e.getLocalizedMessage());
				lbWarn.setVisible(true);
			}
		});
		super.setOnAction(CloseType.APPLY, ev -> {
			logger.log(Level.INFO,"onAction: "+ev);
			try {
				con.login(tfUser2.getText(), tfPass2.getText(), null);
				ret = con.getAccountInfo();
				logger.log(Level.WARNING, "Login into server succeeded");
				credentialsWriter.accept(tfUser.getText(), tfPass.getText());
				lbWarn2.setText(null);
				lbWarn2.setVisible(false);
				getScreenManager().close(this, CloseType.APPLY);
			} catch (EdenAPIException e) {
				logger.log(Level.WARNING, "Login into server failed: "+e.getCode()+" "+e.getLocalizedMessage());
				credentialsWriter.accept(tfUser.getText(), null);
				tfPass2.clear();
				lbWarn2.setText(e.getMessage());
				lbWarn2.setVisible(true);
				buttonDisabledProperty().put(CloseType.RECOVER, false);
			}
		});
		super.setOnAction(CloseType.RECOVER, ev -> {
			logger.log(Level.INFO,"onAction: "+ev);
			try {
				String message = this.con.accountRecover(tfUser2.getText());
				this.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, "Not implemented yet", message+"\n\n"+
				"SORRY! You may have gotten an email, but the last step of entering the code here hasn't been implemented yet. \nThis is not a bug - it is simply not done yet.");
			} catch (EdenAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				this.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, "Not implemented yet", "Sorry, this feature is not present in this release\n"+e.toString());
			}
		});
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbIntro = new Label(ResourceI18N.get(RES, "account.intro"));
		lbIntro.setWrapText(true);
		lbIntro.setStyle("-fx-max-width: 40em");

		lbLink = new Hyperlink(ResourceI18N.get(RES, "account.read_more"));
		lbLink.setOnAction(ev -> {
			String path = "https://rpgframework.atlassian.net/wiki/spaces/COM6/pages/2032926725/Eden+Online+Service";
			BrowserService.create().ifPresent( browser -> {
				try {
					browser.launchExternalBrowser(path);
				} catch (Exception e) {
					e.printStackTrace();
//					logger.log(Level.ERROR, "Error opening {0}", path);
				}
			});

		});

		tfUser = new TextField();
		tfUser.setPromptText(ResourceI18N.get(RES,"field.user.prompt"));
		tfUser.setText(System.getProperty("user.name"));

		tfEmail = new TextField();
		tfEmail.setPromptText(ResourceI18N.get(RES,"field.email.prompt"));
		tfEmail.setText("your@address.com");

		tfFirst = new TextField();
		tfFirst.setPromptText(ResourceI18N.get(RES,"field.first.prompt"));
		tfFirst.setText("John");

		tfLast = new TextField();
		tfLast.setPromptText(ResourceI18N.get(RES,"field.last.prompt"));
		tfLast.setText("Doe");

		cbLoc = new ChoiceBox<>();
		cbLoc.getItems().addAll(Locale.ENGLISH, Locale.GERMAN, Locale.FRENCH, Locale.forLanguageTag("pt"), Locale.forLanguageTag("ru"), Locale.forLanguageTag("jp"));
		if (System.getProperty("user.language")!=null) {
			cbLoc.getSelectionModel().select(Locale.forLanguageTag(System.getProperty("user.language")));
		}

		cbLoc.setConverter(new StringConverter<Locale>() {
			public String toString(Locale loc) {return (loc!=null)?loc.getLanguage():"-";}
			public Locale fromString(String string) {return Locale.getDefault();}
		});

		lbWarn = new Label();
		lbWarn.setWrapText(true);
		lbWarn.setStyle("-fx-text-fill: red");
		VBox.setMargin(lbWarn, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initComponents2() {
		tfUser2 = new TextField();
		tfUser2.setPromptText(ResourceI18N.get(RES,"field.user.prompt"));
		tfUser2.setText(System.getProperty("user.name"));

		tfPass = new PasswordField();
		tfPass2 = new PasswordField();

		lbWarn2 = new Label();
		lbWarn2.setWrapText(true);
		lbWarn2.setStyle("-fx-text-fill: red");
		VBox.setMargin(lbWarn2, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbUser = new Label(ResourceI18N.get(RES,"field.user.label"));
		lbUser.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbEmail = new Label(ResourceI18N.get(RES,"field.email.label"));
		lbEmail.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbFirst = new Label(ResourceI18N.get(RES,"field.first.label"));
		lbFirst.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbPass = new Label(ResourceI18N.get(RES,"field.pass.label"));
		lbPass.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		GridPane grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(10);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.user.label"), tfUser).setTitleMinWidth(150d), 0,0);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.pass.label"), tfPass).setTitleMinWidth(100d), 1,0);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.email.label"), tfEmail).setTitleMinWidth(150d), 0,1);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.lang.label"), cbLoc).setTitleMinWidth(100d), 1,1);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.first.label"), tfFirst).setTitleMinWidth(150d), 0,3);
		grid.add(new TitledComponent(ResourceI18N.get(RES,"field.last.label"), tfLast).setTitleMinWidth(100d), 1,3);
		VBox layout = new VBox(lbIntro, lbLink, grid, lbWarn);

		lbWarn.setVisible(false);

		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initLayout2() {
		Label lbUser2 = new Label(ResourceI18N.get(RES,"field.user.label"));
		lbUser2.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbPass2 = new Label(ResourceI18N.get(RES,"field.pass.label"));
		lbPass2.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		GridPane grid2 = new GridPane();
		grid2.setVgap(5);
		grid2.setHgap(10);
		grid2.add(lbUser2, 0, 0);
		grid2.add(tfUser2 , 1, 0);
		grid2.add(lbPass2, 0, 1);
		grid2.add(tfPass2, 1, 1);
		VBox layout2 = new VBox(grid2, lbWarn2);

		lbWarn2.setVisible(false);

		setContent2(layout2);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfUser.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<4) {
				tfUser.getStyleClass().add("invalid-content");
			} else {
				tfUser.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfUser2.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<4) {
				tfUser.getStyleClass().add("invalid-content");
			} else {
				tfUser.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfEmail.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<4 || !n.contains("@") || !n.contains(".")) {
				tfEmail.getStyleClass().add("invalid-content");
			} else {
				tfEmail.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfPass.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<6) {
				tfPass.getStyleClass().add("invalid-content");
			} else {
				tfPass.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfPass2.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<6) {
				tfPass.getStyleClass().add("invalid-content");
			} else {
				tfPass.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
	}

	//-------------------------------------------------------------------
	private boolean isDataValid() {
		String user = tfUser.getText();
		String pass = tfPass.getText();
		String mail = tfEmail.getText();
		String first = tfFirst.getText();
		String last = tfLast.getText();
		if (user==null || user.length()<4 )
			return false;
		if (pass==null || pass.length()<6 )
			return false;
		if (mail==null || mail.length()<4 || !mail.contains("@") || !mail.contains("."))
			return false;
		if (first==null || first.length()<2 )
			return false;
		if (last==null || last.length()<2 )
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	private void refreshButtons() {
		boolean valid = isDataValid();
		buttonDisabledProperty().put(CloseType.YES, !valid);
		buttonDisabledProperty().put(CloseType.APPLY, tfPass2.getText().isBlank());
	}

	//-------------------------------------------------------------------
	public String getLogin()    { return getTab2Visible()?tfUser2.getText():tfUser.getText(); }
	public String getPassword() { return getTab2Visible()?tfPass2.getText():tfPass.getText(); }
	public EdenAccountInfo getAccountInfo() { return ret; }
	//-------------------------------------------------------------------
	public void setLogin(String value) { tfUser.setText(value); refreshButtons();}
	public void setPassword(String value) { tfPass.setText(value); refreshButtons();}

}
