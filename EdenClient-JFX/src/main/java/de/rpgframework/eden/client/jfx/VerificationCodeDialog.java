package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenConnection;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class VerificationCodeDialog extends ManagedDialog {
	
	private final static Logger logger = System.getLogger("eden.client");

	private final static ResourceBundle RES = ResourceBundle.getBundle(LoginDialog.class.getPackageName()+".Dialogs");
	
	private String email;
	private TextField[] tfCodes;
	private Label lbWarn;

	//-------------------------------------------------------------------
	/**
	 */
	public VerificationCodeDialog(String email, EdenConnection con, int length) {
		super(ResourceI18N.get(RES, "verification.title"), null, CloseType.CANCEL, CloseType.APPLY);
		this.email = email;
		initComponents(length);
		initLayout();
		super.setOnAction(CloseType.APPLY, ev -> {
			logger.log(Level.INFO,"onAction: "+ev);
			try {
				con.verifyAccount(getCode());
				lbWarn.setText(null);
				lbWarn.setVisible(false);
				getScreenManager().close(this, CloseType.YES);
			} catch (EdenAPIException e) {
				logger.log(Level.ERROR, "Signal "+e.getMessage()+" / "+e.getLocalizedMessage());
				lbWarn.setText(e.getLocalizedMessage());
				lbWarn.setVisible(true);
			}
		});
	}

	//-------------------------------------------------------------------
	private void initComponents(int length) {
		tfCodes = new TextField[length];
		for (int i=0; i<tfCodes.length; i++) {
			tfCodes[i] = new TextField();
			tfCodes[i].setPrefColumnCount(1);
			tfCodes[i].setStyle("-fx-font-size: 400%");
			tfCodes[i].setTextFormatter(new TextFormatter<String>((Change change) -> {
			    String newText = change.getControlNewText();
			    if (newText.length() > 1) {
			        return null ;
			    } else {
			        return change ;
			    }
			}));
			if ((i+1)<tfCodes.length)
				tfCodes[i].setUserData(tfCodes[i+1]);
			int current = i;
			tfCodes[i].textProperty().addListener( (ov,o,n) -> charEntered(o,n,current));
		}
		lbWarn = new Label();
		lbWarn.setWrapText(true);
		lbWarn.setStyle("-fx-text-fill: red");
		VBox.setMargin(lbWarn, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbMess = new Label(ResourceI18N.format(RES, "verification.mess", email));
		lbMess.setWrapText(true);
		
		HBox line = new HBox(20);
		for (int i=0; i<tfCodes.length; i++) {
			line.getChildren().addAll(tfCodes[i]);
		}
		
		VBox layout = new VBox(20, lbMess, line, lbWarn);
		lbWarn.setVisible(false);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void charEntered(String o, String n, int current) {
		boolean forward = n.length()>o.length();
		if (forward) {
			int next = current+1;
			if (next<tfCodes.length)
				tfCodes[next].requestFocus();
		} else {
			int next = current-1;
			if (next>=0)
				tfCodes[next].requestFocus();
		}
	}

	//-------------------------------------------------------------------
	public String getCode() {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<tfCodes.length; i++) {
			buf.append(tfCodes[i].getText());
		}
		return buf.toString();
	}

}
