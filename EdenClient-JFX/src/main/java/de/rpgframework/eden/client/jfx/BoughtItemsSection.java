package de.rpgframework.eden.client.jfx;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import com.gluonhq.attach.browser.BrowserService;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.TilePane;
import javafx.scene.text.TextAlignment;

/**
 *
 */
public class BoughtItemsSection extends ListSection<BoughtItem> {

	private final static Logger logger = System.getLogger(BoughtItemsSection.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(BoughtItemsSection.class.getPackageName()+".Sections");

	private EdenClientApplication app;
	private RoleplayingSystem rules;
	protected Button btnBuy;
	protected Button btnActivate;
	protected Function<BoughtItem, CatalogItem> resolver;
	protected Function<UUID, Boolean> activateFunction;

	//-------------------------------------------------------------------
	public BoughtItemsSection(EdenClientApplication app, RoleplayingSystem rules) {
		super(ResourceI18N.get(RES, "section.boughtItems.title"));
		this.rules = rules;
		this.app   = app;

		initHeaderNode();
		initInteractivity();
		Label placeholder = new Label(ResourceI18N.get(RES, "section.boughtItems.placeholder"));
		placeholder.setTextAlignment(TextAlignment.CENTER);
		list.setPlaceholder(placeholder);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initHeaderNode() {
		btnBuy      = new Button(ResourceI18N.get(RES, "section.boughtItems.buy"));
		btnBuy.getStyleClass().add("large-button1");
		btnBuy.setMaxWidth(Double.MAX_VALUE);
		btnBuy.setTooltip(new Tooltip(ResourceI18N.get(RES, "section.boughtItems.buy.help")));
		btnActivate = new Button(ResourceI18N.get(RES, "section.boughtItems.activate"));
		btnActivate.getStyleClass().add("large-button2");
		btnActivate.setMaxWidth(Double.MAX_VALUE);
		btnActivate.setTooltip(new Tooltip(ResourceI18N.get(RES, "section.boughtItems.activate.help")));

		TilePane buttons = new TilePane();
		buttons.setAlignment(Pos.CENTER);
		buttons.setMaxWidth(Double.MAX_VALUE);
		buttons.getChildren().addAll(btnBuy, btnActivate);
		buttons.setHgap(20);
		buttons.setVgap(20);
		buttons.setTileAlignment(Pos.CENTER);
		setHeaderNode(buttons);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnBuy.setOnAction(ev -> onBuy());
		btnActivate.setOnAction(ev -> onActivate());
	}


	@Override
	protected void onAdd() {
		logger.log(Level.WARNING, "TODO: onAdd");
	}

	@Override
	protected void onDelete(BoughtItem item) {
		logger.log(Level.WARNING, "TODO: onDelete");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		super.refresh();
		btnBuy.setDisable(app.getAccountInfo()==null);
		btnActivate.setDisable(app.getAccountInfo()==null);
		if (app.getEdenConnection()==null || app.isOffline()) {
			list.getItems().clear();
		} else
			setData( app.getEdenConnection().getContentPacks(rules) );
	}

	//-------------------------------------------------------------------
	private void onBuy() {
		logger.log(Level.INFO, "onBuy");
		String url = app.getLicenseURL(); //ResourceI18N.get(RES, "section.boughtItems.buy.url");
		logger.log(Level.INFO, "Visit "+url);
		BrowserService.create().ifPresent(service -> {
		      try {
				service.launchExternalBrowser(url);
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  });
	}

	//-------------------------------------------------------------------
	private void onActivate() {
		logger.log(Level.ERROR, "onActivate");
		TextField tfUUID = new TextField();
		tfUUID.setPrefColumnCount(24);
		tfUUID.setPromptText("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");

		ManagedDialog dialog = new ManagedDialog(
			ResourceI18N.get(RES, "section.boughtItems.activate.title"),
			tfUUID,
			CloseType.OK, CloseType.CANCEL
		);

		NavigButtonControl btnCtrl = new NavigButtonControl();
		tfUUID.textProperty().addListener( (ov,o,n) -> {
			// Validate input
			logger.log(Level.ERROR, "UUID: "+n);
			if (n!=null && !n.isEmpty()) {
				try {
					UUID.fromString(tfUUID.getText());
					tfUUID.getStyleClass().remove("text-field.invalid-content");
					btnCtrl.setDisabled(CloseType.OK, false);
					logger.log(Level.ERROR, "OKay");
				} catch (IllegalArgumentException e) {
					logger.log(Level.ERROR, "Not OKay");
					btnCtrl.setDisabled(CloseType.OK, true);
					if (!tfUUID.getStyleClass().contains("text-field.invalid-content"))
						tfUUID.getStyleClass().add("text-field.invalid-content");
				}
			} else
				btnCtrl.setDisabled(CloseType.OK, true);

		});

		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, btnCtrl);

		//CloseType closed = AlertManager.showAlertAndCall("Enter license", tfUUID, CloseType.OK, CloseType.CANCEL);
		System.err.println("Closed via "+closed);
		if (closed==CloseType.OK) {
			UUID uuid = UUID.fromString(tfUUID.getText());
			boolean success = activateFunction.apply(uuid);
			setData( app.getEdenConnection().getContentPacks(rules) );
		}
	}

}
