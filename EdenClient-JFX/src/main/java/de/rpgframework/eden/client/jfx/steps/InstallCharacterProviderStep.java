package de.rpgframework.eden.client.jfx.steps;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;

import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.SyncingCharacterProvider;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.EdenSettings;

/**
 * @author prelle
 *
 */
public class InstallCharacterProviderStep implements StartupStep {

	protected static Logger logger = System	.getLogger(InstallCharacterProviderStep.class.getPackageName());

	private EdenClientApplication parent;
	private RoleplayingSystem rules;

	//-------------------------------------------------------------------
	public InstallCharacterProviderStep(EdenClientApplication parent, RoleplayingSystem rules) {
		this.parent = parent;
		this.rules  = rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return parent.getEdenConnection()!=null;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		/*
		 * 1. Determine local data directory
		 */
		Path playerDir = EdenSettings.appDir.resolve("player");
		logger.log(Level.INFO, "Use "+playerDir+" as data directory");
		try {
			if (!Files.exists(playerDir))
				Files.createDirectories(playerDir);
		} catch (IOException e1) {
			logger.log(Level.ERROR, "Failed creating player dir: ",e1);
		}
		System.setProperty("eden.playerdir", playerDir.toAbsolutePath().toString());

		/*
		 * 2. Prepare character provider
		 */
		SyncingCharacterProvider<?> prov = null;
		try {
			prov = new SyncingCharacterProvider(playerDir, parent.getEdenConnection(), rules);
			CharacterProviderLoader.setCharacterProvider(prov);
		} catch (CharacterIOException e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Error setting up character provider",e);
			parent.handleError(e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Unknown Error setting up character provider",e);
			parent.handleError(e);
		}
	}

}
