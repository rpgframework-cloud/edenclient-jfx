package de.rpgframework.eden.client.jfx.steps;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.api.EdenPingInfo;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.jfx.EdenClientApplication;

/**
 * @author prelle
 *
 */
public class CheckServerOnlineStep implements StartupStep, EdenClientConstants {

	protected static Logger logger = System	.getLogger(CheckServerOnlineStep.class.getPackageName());

	private EdenClientApplication parent;

	//-------------------------------------------------------------------
	public CheckServerOnlineStep(EdenClientApplication parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			EdenConnection eden = parent.getEdenConnection();
			EdenPingInfo ping = eden.getInfo();
			logger.log(Level.INFO, "Eden server is {0}",  (ping!=null)?"online":"offline");
			parent.setEdenPingInfo(ping);
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return parent.getEdenConnection()!=null;
	}

}
