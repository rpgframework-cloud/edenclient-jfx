package de.rpgframework.eden.client.jfx;

import java.io.File;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.GenericCore;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * @author prelle
 *
 */
public class PDFPage extends Page  {

	public final static ResourceBundle RES = ResourceBundle.getBundle(PDFPage.class.getPackageName()+".EdenPages");

	private final static Logger logger = System.getLogger(PDFPage.class.getPackageName());

	private EdenClientApplication app;
	private RoleplayingSystem rules;

	private ToggleSwitch cbEnable;
	private GridPane grid;
	private Label lbInfo;

	private File lastDir;

	//-------------------------------------------------------------------
	public PDFPage(EdenClientApplication mainApp, RoleplayingSystem rules) {
		super(ResourceI18N.get(RES,"page.pdf.title"));
		this.app = mainApp;
		this.rules = rules;
		initComponents();
		initLayout();
		String tmp = Preferences.userNodeForPackage(getClass()).get("lastDir",null);
		if (tmp!=null)
			lastDir = new File(tmp);
		if (lastDir!=null && !lastDir.exists())
			lastDir = null;
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbInfo = new Label(ResourceI18N.get(RES, "page.pdf.infotext"));
		lbInfo.setWrapText(true);
		cbEnable = new ToggleSwitch(ResourceI18N.get(RES, "page.pdf.enable"));
		cbEnable.setSelected(app.isPDFEnabled());
		cbEnable.selectedProperty().addListener( (ov,o,n) -> {
			app.setPDFEnable(n);
		});
		grid = new GridPane();
		grid.setVgap(5);

		int y=0;
		for (DataSet set : GenericCore.getDataSets()) {
			if (set.getRules()!=rules)
				continue;
			String name = set.getName(Locale.getDefault());
			Label lbName = new Label(name);
			lbName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
			grid.add(lbName, 0, y);

			TextField tfPath = new TextField();
			tfPath.setEditable(false);
			tfPath.setUserData(set);
			tfPath.setPrefColumnCount(40);
			tfPath.setText(app.getPDFPathFor(set.getRules(), set.getID(), Locale.getDefault().getLanguage()).path);
			Button btnChooser = new Button(ResourceI18N.get(RES, "page.pdf.button.select"), new SymbolIcon("OpenFile"));
			Button btnDelete  = new Button(ResourceI18N.get(RES, "page.pdf.button.clear"), new SymbolIcon("Delete"));
			btnChooser.setTooltip(new Tooltip(ResourceI18N.get(RES, "page.pdf.button.select.tooltip")));
			btnDelete.setTooltip(new Tooltip(ResourceI18N.get(RES, "page.pdf.button.clear.tooltip")));
//			btnDelete.setOnAction(ev -> ReferencePDFViewer.);
			TextField tfOffset = new TextField();
			tfOffset.setPrefColumnCount(3);
			tfOffset.setMinWidth(35);
			tfOffset.setText(String.valueOf( app.getPDFOffsetFor(set.getRules(), set.getID(), Locale.getDefault().getLanguage() )));
			tfOffset.textProperty().addListener( (ov,o,n) -> offsetChanged(set, tfPath,tfOffset));
			tfOffset.setText(app.getPDFPathFor(set.getRules(), set.getID(), Locale.getDefault().getLanguage()).offset+"");
			HBox line = new HBox(5, tfPath, btnChooser, btnDelete, tfOffset);
			if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
				grid.add(line, 0, ++y);
				btnChooser.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				btnDelete.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			} else
				grid.add(line, 1, y);

			// Open FileChooser
			btnChooser.setOnAction(ev -> chooseFileFor(set, tfPath, tfOffset));
			btnDelete.setOnAction(ev -> {
				tfPath.clear();
				tfOffset.clear();
				app.setPDFPathFor(rules, set.getID(), Locale.getDefault().getLanguage(), null, 0);
			});

			y++;
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		ScrollPane scroll = new ScrollPane(grid);
		scroll.setFitToWidth(true);
		VBox layout = new VBox(10, lbInfo, cbEnable, scroll);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		super.setResponsiveMode(value);

		for (Node node : grid.getChildren()) {
			if (node instanceof Button) {
				((Button)node).setContentDisplay( (value==WindowMode.MINIMAL)?ContentDisplay.GRAPHIC_ONLY:ContentDisplay.LEFT);
				logger.log(Level.INFO, "Change content display of "+node+" to "+((Button)node).getContentDisplay());
			}
		}
		requestLayout();
	}

	//-------------------------------------------------------------------
	private void chooseFileFor(DataSet set, TextField tfPath, TextField tfOffset) {
		logger.log(Level.INFO, "Open file chooser for last directory ''{0}''", lastDir);
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(new ExtensionFilter("PDF", "*.pdf"));
		try {
			if (lastDir!=null)
				chooser.setInitialDirectory(lastDir);
			if (tfPath.getText()!=null) {
				Path toCheck = Paths.get(tfPath.getText());
				if (Files.exists(toCheck)) {
					chooser.setInitialFileName(tfPath.getText());
				}
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error configuring file chooser",e);
		}
		File selected = chooser.showOpenDialog(FlexibleApplication.getInstance().getAppLayout().getScene().getWindow());
		if (selected!=null) {
			logger.log(Level.INFO, "Set PDF path of ''{0}'' to ''{1}''", set.getID(), selected);
			lastDir = selected.getParentFile();
			Preferences.userNodeForPackage(getClass()).put("lastDir", lastDir.getAbsolutePath());
			tfPath.setText(selected.getAbsolutePath());
			int offset = 0;
			try { offset = Integer.parseInt(tfOffset.getText()); } catch (NumberFormatException e) {}
			app.setPDFPathFor(rules, set.getID(), Locale.getDefault().getLanguage(), selected.getAbsolutePath(), offset);
		}

	}

	//-------------------------------------------------------------------
	private void offsetChanged(DataSet set, TextField tfPath, TextField tfOffset) {
		String path = tfPath.getText();
		logger.log(Level.INFO, "Set PDF path of ''{0}'' to ''{1}''", set.getID(), path);
		int offset = 0;
		try {offset = Integer.parseInt(tfOffset.getText()); } catch (NumberFormatException e) {}
		app.setPDFPathFor(rules, set.getID(), Locale.getDefault().getLanguage(), path, offset);
	}

}
