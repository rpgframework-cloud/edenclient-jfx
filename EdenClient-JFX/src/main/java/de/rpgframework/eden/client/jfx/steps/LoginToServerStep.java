package de.rpgframework.eden.client.jfx.steps;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.util.Services;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenCodes;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.InteractiveAuthenticator;
import de.rpgframework.eden.client.jfx.AskForPasswordAuthenticator;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import javafx.application.Platform;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class LoginToServerStep implements StartupStep, EdenClientConstants {

	protected static Logger logger = System	.getLogger(LoginToServerStep.class.getPackageName());

	private EdenClientApplication parent;
	private SettingsService service;

	//-------------------------------------------------------------------
	public LoginToServerStep(EdenClientApplication parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * Read user credentials from SettingService - if present - or from
	 * Preferences otherwise
	 */
	private String[] readEdenCredentials() {
		String user = service.retrieve(PREF_USER);
		String pass = service.retrieve(PREF_PASS);
		return new String[] {user,pass};
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private void writeEdenCredentials(String user, String pass) {
		SettingsService service = Services.get(SettingsService.class).get();
		service.store(PREF_USER, user);
		service.store(PREF_PASS, pass);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			EdenConnection eden = parent.getEdenConnection();
			InteractiveAuthenticator interactiveAuth = new AskForPasswordAuthenticator( ()->parent.getEdenConnection(), (l,p)->writeEdenCredentials(l,p), parent.getSecurityDialogImage()) ;
			eden.getAuthenticator().setInteractiveFallback(interactiveAuth);
			// Configure cached credentials
			String[] credentials = readEdenCredentials();
			eden.login(credentials[0], credentials[1], interactiveAuth);
			// Now get account information
			try {
				logger.log(Level.INFO, "Trying to blockingly access server while in Thread "+Thread.currentThread());
				EdenAccountInfo info = eden.getAccountInfo();
				parent.setAccountInfo(info);
				logger.log(Level.INFO, "Successfully logged into server - user is {0} {1}", info.getFirstName(), info.getLastName());
			} catch (EdenAPIException e) {
				logger.log(Level.ERROR, "Failed to log into server: "+e.getCode(),e);
				if (e.getCode()==EdenCodes.UNAUTHORIZED) {
					service.remove(PREF_USER);
					service.remove(PREF_PASS);
					service.store(CheckAccountCreationStep.CFG_ALREADY_ASKED_ACCOUNT, "false");
				} else
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 3, "Failed to log into server: "+e.getMessage());
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		if (parent.isOffline()) {
			logger.log(Level.INFO, "No login attempt, since server could not be contacted");
			return false;
		}

		service = Services.get(SettingsService.class).orElse(null);
		if (service==null) {
			logger.log(Level.ERROR, "No SettingService available");
			return false;
		}
		String login = readEdenCredentials()[0];
		if (login==null) {
			logger.log(Level.WARNING, "No login configured");
		}
		return parent.getEdenConnection()!=null && readEdenCredentials()[0]!=null;
	}

}
