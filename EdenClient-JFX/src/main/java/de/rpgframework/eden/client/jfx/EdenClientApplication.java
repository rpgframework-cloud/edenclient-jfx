package de.rpgframework.eden.client.jfx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.function.Consumer;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.Page;
import org.prelle.javafx.SymbolIcon;

import com.gluonhq.attach.browser.BrowserService;
import com.gluonhq.attach.device.DeviceService;
import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.storage.StorageService;
import com.gluonhq.attach.util.Services;
import com.itextpdf.text.pdf.PdfReader;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenPingInfo;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.EdenConnection.StateFlag;
import de.rpgframework.eden.client.OnlineStartupStep;
import de.rpgframework.eden.client.SynchronizeCharactersStep;
import de.rpgframework.eden.client.SyncingCharacterProvider;
import de.rpgframework.eden.client.jfx.steps.CheckAccountCreationStep;
import de.rpgframework.eden.client.jfx.steps.CheckForUpdatesStep;
import de.rpgframework.eden.client.jfx.steps.CheckServerOnlineStep;
import de.rpgframework.eden.client.jfx.steps.InstallCharacterProviderStep;
import de.rpgframework.eden.client.jfx.steps.InstallResourceManagerStep;
import de.rpgframework.eden.client.jfx.steps.LoginToServerStep;
import de.rpgframework.eden.client.jfx.steps.PrepareEdenConnectionStep;
import de.rpgframework.eden.client.jfx.steps.SynchronizeLicensesStep;
import de.rpgframework.eden.client.jfx.steps.VerifyPlayerStep;
import de.rpgframework.jfx.attach.PDFViewerConfig;
import de.rpgframework.jfx.attach.PDFViewerConfig.PathAndOffset;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public abstract class EdenClientApplication extends FlexibleApplication implements EdenClientConstants, Consumer<List<StateFlag>> {

	private static String EDEN_SERVER = "localhost";

	protected static Logger logger = System	.getLogger(EdenClientApplication.class.getPackageName());
	private static String PREF_MODULES = "eden.modules";
	public static String PREF_LANG = "eden.lang";

	private ResourceBundle RES = ResourceBundle.getBundle(EdenClientApplication.class.getName(), EdenClientApplication.class.getModule());
	private RoleplayingSystem rules;
	private String appName;
	private String scheme = "https";

	private String licenseURL;
	protected EdenConnection eden;
	protected EdenPingInfo edenInfo;
	protected EdenAccountInfo accountInfo;
	protected String[] allowedDatasets;

	protected MenuItem navigChars;
	protected MenuItem navigLookup;
	protected MenuItem navigAccount;
	protected MenuItem navigPDF;
	protected MenuItem navigAbout;
	protected VBox bxFooter;
	private Label lbLoggedInUser;

	private Thread afterStart;

//	protected List<StartupStep> preGUISteps, postGUISteps;

    //-------------------------------------------------------------------
    public EdenClientApplication() {}

    //-------------------------------------------------------------------
	public EdenClientApplication(RoleplayingSystem rules, String appName) {
		this.rules = rules;
		this.appName = appName;
		PREF_MODULES = PREF_MODULES+"."+rules.name().toLowerCase();
		PREF_LANG = appName+".lang";
		licenseURL = ResourceI18N.get(RES, "license.url."+appName.toLowerCase());
	}

	//-------------------------------------------------------------------
	public EdenConnection getEdenConnection() {
		return eden;
	}

	//-------------------------------------------------------------------
	public void setEdenConnection(EdenConnection eden) {
		this.eden = eden;
		eden.addListener(this);
	}

	//-------------------------------------------------------------------
	public RoleplayingSystem getRules() {
		return rules;
	}

	//-------------------------------------------------------------------
	public void runStartupStepsBlocking(List<StartupStep> steps) {
		for (StartupStep step : steps) {
			int num = steps.indexOf(step)+1;
			if (step instanceof OnlineStartupStep) {
				((OnlineStartupStep)step).updateConnection(eden);
			}
			if (step.canRun()) {
				logger.log(Level.WARNING, "--STEP {0}: {1}----",num,step.getClass().getSimpleName()+"  "+Thread.currentThread());
				try {
					step.run();
				} catch (Exception e) {
					e.printStackTrace();
					logger.log(Level.ERROR, "ERROR in step "+step.getClass().getSimpleName(),e);
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1,
							ResourceI18N.format(RES, "error.startup_step", step.getClass().getSimpleName(), e.toString()));
				}
			} else {
				logger.log(Level.WARNING, "--STEP {0}: {1}----CANNOT RUN",num,step.getClass().getSimpleName()+"  "+Thread.currentThread());
			}
		}
	}

	//-------------------------------------------------------------------
	protected List<StartupStep> getPreGUISteps() {
		return List.of(new StartupStep[] {
				new PrepareEdenConnectionStep(this, appName, EDEN_SERVER, scheme),
				new InstallCharacterProviderStep(this, rules),
				new InstallResourceManagerStep(this, rules),
				new CheckServerOnlineStep(this),
		});
	}

	//-------------------------------------------------------------------
	public List<StartupStep> getPostGUISteps() {
		return List.of(new StartupStep[] {
			new CheckForUpdatesStep(this, appName),
				new CheckAccountCreationStep(this),
				new LoginToServerStep(this),
				new VerifyPlayerStep(this),
		});
	}

	//-------------------------------------------------------------------
	public List<StartupStep> getAfterVerifiedLoginSteps() {
		return List.of(new StartupStep[] {
			new CheckForUpdatesStep(this, appName),
				new SynchronizeLicensesStep(rules),
				new SynchronizeCharactersStep(),
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.FlexibleApplication#init()
	 */
	@Override
	public void init() {
		bxFooter = new VBox(5);
		lbLoggedInUser = new Label();
		lbLoggedInUser.setMaxWidth(195);
		bxFooter.getChildren().add(lbLoggedInUser);
		VBox.setMargin(lbLoggedInUser, new Insets(0, 0, 0, 5));

		String profile = System.getProperty("profile","");
		Parameters params = getParameters();
		if (params!=null && params.getNamed().containsKey("profile")) {
			profile = params.getNamed().get("profile");
		}
		logger.log(Level.INFO, "Profile: ''{0}''", profile);

		scheme= "https";
		switch (profile.toLowerCase()) {
		case "":
			EDEN_SERVER = "192.168.0.2";
//			scheme= "http";
			break;
		case "develop":
			EDEN_SERVER = "eden-test.rpgframework.de";
			break;
		case "testing":
			EDEN_SERVER = "eden-test.rpgframework.de";
			break;
		case "stable":
			EDEN_SERVER = "eden.rpgframework.de";
			break;
		default:
			EDEN_SERVER = profile;
		}
		logger.log(Level.INFO, "Set EDEN_SERVER to '"+EDEN_SERVER+"'");

//		preGUISteps = getPreGUISteps();
//		postGUISteps = getPostGUISteps();
//

	    logger.log(Level.INFO, "Version: {0}", System.getProperty("project.version", "NO VERSION INFO"));
	    logger.log(Level.INFO, "Time   : {0}", String.valueOf(Instant.now()));
		DeviceService.create().ifPresent(service -> {
		      logger.log(Level.INFO, "Device Model Name: {0}", service.getModel());
		      logger.log(Level.INFO, "Device Platform Name: {0}", service.getPlatform());
		      logger.log(Level.INFO, "Device Platform Version: {0}", service.getVersion());
		      logger.log(Level.INFO, "Device UUID: {0}", service.getUuid());
		      System.out.printf("Device Model Name: %s\n", service.getModel());
		      System.out.printf("Device Platform Name: %s\n", service.getPlatform());
		      System.out.printf("Device Platform Version: %s\n", service.getVersion());
		      System.out.printf("Device UUID: %s\n", service.getUuid());
		  });
        registerListener();
//        interactiveAuth = new AskForPasswordAuthenticator( ()->eden, (l,p)->writeEdenCredentials(l,p)) ;
//		setAuthViaLoginDialog();
        //stepCharacterProvider();


        runStartupStepsBlocking(getPreGUISteps());
		logger.log(Level.INFO, "Creating afterStart with {0}", Thread.currentThread());
		afterStart = new Thread(()->{
			logger.log(Level.INFO, "afterStart with "+Thread.currentThread());
			runStartupStepsBlocking(getPostGUISteps());
		}, "afterStart");

	}

	//-------------------------------------------------------------------
	/**
	 * Read user credentials from SettingService - if present - or from
	 * Preferences otherwise
	 */
	private String[] readEdenCredentials() {
		SettingsService service = Services.get(SettingsService.class).orElse(null);
		if (service!=null) {
			String user = service.retrieve(PREF_USER);
			String pass = service.retrieve(PREF_PASS);
			return new String[] {user,pass};
		}
		return new String[2];
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private void writeEdenCredentials(String user, String pass) {
		SettingsService service = Services.get(SettingsService.class).get();
		if (service!=null) {
			if (user!=null) service.store(PREF_USER, user); else service.remove(PREF_USER);
			if (pass!=null) service.store(PREF_PASS, pass); else service.remove(PREF_PASS);
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			pref.put(PREF_USER, user);
			pref.put(PREF_PASS, pass);
		}
	}

	//-------------------------------------------------------------------
	private final String[] readLicensesModules() {
		SettingsService service = Services.get(SettingsService.class).orElse(null);
		if (service!=null) {
			String data = service.retrieve(PREF_MODULES);
			if (data==null)
				return new String[0];
			return data.split(",");
		}
		return new String[0];
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private final String[] writeLicensedModules(List<String> modules) {
		if (modules==null) {
			logger.log(Level.ERROR, "writeLicensedModules() received a null pointer");
			return new String[0];
		}
		SettingsService service = Services.get(SettingsService.class).get();
		String[] ret = modules.toArray(new String[modules.size()]);
		if (service!=null) {
			service.store(PREF_MODULES, String.join(",", modules));
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			pref.put(PREF_MODULES, String.join(",", modules));
		}
		return ret;
	}

    //-------------------------------------------------------------------
	protected ResourceBundle getErrorDialogResourceBundle() {
		return RES;
	}

    //-------------------------------------------------------------------
	protected Image getLoginDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	protected Image getInfoDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	protected Image getWarningDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	protected Image getErrorDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	protected Image getUpdateDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	public Image getSecurityDialogImage() {
		return null;
	}

    //-------------------------------------------------------------------
	protected void showUIMessage(int type, String mess, Throwable cause, Path file) {
		ResourceBundle RES = getErrorDialogResourceBundle();
		String title = ResourceI18N.get(RES, "uimessage.info");
		switch (type) {
		case 0: title = ResourceI18N.get(RES, "uimessage.info"); break;
		case 1: title = ResourceI18N.get(RES, "uimessage.warning"); break;
		case 2: title = ResourceI18N.get(RES, "uimessage.error"); break;
		case 3: title = ResourceI18N.get(RES, "uimessage.security"); break;
		}

		Label content= new Label(mess);
		content.setWrapText(true);
		content.getStyleClass().add("text-body");

		Button btnOpenLogDir = new Button(ResourceI18N.get(RES, "button.openLogDir"));
		btnOpenLogDir.setOnAction(ev -> {
			Path logDir = Paths.get(System.getProperty("logdir"));
			openFileBrowser(logDir);
		});

		VBox layout = new VBox(10, content);
		Accordion accord = new Accordion();
		if (cause!=null) {
			StringWriter out = new StringWriter();
			cause.printStackTrace(new PrintWriter(out));
			TextArea taTrace = new TextArea(out.toString());
			taTrace.setPrefColumnCount(40);
			TitledPane tpTrace = new TitledPane(ResourceI18N.get(RES, "uimessage.stacktrace"), taTrace);
			tpTrace.setStyle("-fx-font-size: 100%");
			accord.getPanes().add(tpTrace);
		}
		if (file != null) {
			TextField tfPath = new TextField(file.toAbsolutePath().toString());
			tfPath.setPrefColumnCount(40);
			Button btnOpen = new Button(null, new SymbolIcon("edit"));
			btnOpen.getStyleClass().add("mini-button");
			btnOpen.setOnAction(ev -> BabylonEventBus.fireEvent(BabylonEventType.OPEN_FILE, file));
			TitledPane tpPath = new TitledPane(ResourceI18N.get(RES, "uimessage.file"),new HBox(5, btnOpen,tfPath));
			tpPath.setStyle("-fx-font-size: 100%");
			accord.getPanes().add(tpPath);
		}
		if (!accord.getPanes().isEmpty()) {
			layout.getChildren().add(accord);
		}
		if (type==2)
			layout.getChildren().add(btnOpenLogDir);

		ManagedDialog dialog = new ManagedDialog(title, layout, CloseType.OK);
		switch (type) {
		case 0: dialog.setImage(getInfoDialogImage()); break;
		case 1: dialog.setImage(getWarningDialogImage()); break;
		case 2: dialog.setImage(getErrorDialogImage()); break;
		case 3: dialog.setImage(getSecurityDialogImage()); break;
		}
		showAlertAndCall(dialog, null);
	}

    //-------------------------------------------------------------------
	public void showUpdateMessage(String installed, String available, LocalDate date, String url, String down) {
		ResourceBundle RES = getErrorDialogResourceBundle();
		String title = ResourceI18N.get(RES, "uimessage.update.title");
		String desc = ResourceI18N.format(RES, "uimessage.update.desc", available, date, installed);
		String downLink = ResourceI18N.get(RES, "uimessage.update.download");

		Label content= new Label(desc);
		content.setWrapText(true);
		content.getStyleClass().add("text-body");


		VBox layout = new VBox(10, content);

		if (url!=null && !url.isBlank() && BrowserService.create().isPresent()) {
			Hyperlink hyper = new Hyperlink(ResourceI18N.get(RES, "uimessage.update.link"));
			hyper.setOnAction(ev -> {
				logger.log(Level.INFO, "Open "+url);
				BrowserService.create().ifPresent( browser -> {
					try {
						browser.launchExternalBrowser(url);
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.ERROR, "Error opening {0}", url);
					}
				});

			});
			layout.getChildren().add(hyper);
		}

		if (down!=null && !down.isBlank() && BrowserService.create().isPresent()) {
			Hyperlink hyper = new Hyperlink(downLink);
			hyper.setOnAction(ev -> {
				logger.log(Level.INFO, "Open "+down);
				BrowserService.create().ifPresent( browser -> {
					try {
						browser.launchExternalBrowser(down);
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.ERROR, "Error opening {0}", url);
					}
				});

			});
			layout.getChildren().add(hyper);
		}

		ManagedDialog dialog = new ManagedDialog(title, layout, CloseType.OK);
		dialog.setImage(getUpdateDialogImage());

		showAlertAndCall(dialog, null);
	}

    //-------------------------------------------------------------------
	public void openFileBrowser(Path path) {
		String osName = System.getProperty("os.name").toLowerCase();
		logger.log(Level.INFO, "Open file for {0} : {1}", osName, path);
		try {
			if (osName.contains("windows")) {
				// See https://ss64.com/nt/explorer.html
				Runtime.getRuntime().exec(new String[]{"explorer",path.toAbsolutePath().toString()});
			} else if (osName.contains("mac")) {
				// See https://scriptingosx.com/2017/02/the-macos-open-command/
				Runtime.getRuntime().exec(new String[]{"open","-R",path.toAbsolutePath().toString()});
			} else if (osName.contains("linux")) {
				openFile(path);
//				Runtime.getRuntime().exec("xdg-open "+path.toAbsolutePath());
			} else {
				logger.log(Level.WARNING, "Not supported: Open file for {0} : {1}", osName, path);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    //-------------------------------------------------------------------
    /**
     * @throws IOException
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    public void start(Stage stage) throws Exception {
    	super.start(stage);
    	logger.log(Level.ERROR,"---------------------------PDFViewing = "+PDFViewerServiceFactory.create().isPresent());
    	stepPages();
        getAppLayout().getNavigationPane().setSettingsVisible(false);

        setStyle(stage.getScene(), FlexibleApplication.LIGHT_STYLE);

        //stepAccount();
//
        PDFViewerConfig.setEnabled(isPDFEnabled());

		configureDragAndDrop(stage.getScene());

//		logger.log(Level.INFO, "Now start {0}", afterStart);
//		afterStart.start();

		logger.log(Level.INFO, "Check connection");
		eden.addListener(this);
//		checkAccountToDo();
//        getAppLayout().visibleProperty().addListener( (ov,o,n) -> {
//        	logger.log(Level.INFO, "Visibility changed to "+n+"-------------------");
//        	afterStart.start();
//        });
    	afterStart.start();
		logger.log(Level.INFO, "LEAVE start (thread {0})", Thread.currentThread());
   }

	//-------------------------------------------------------------------
    private void registerListener() {
		BabylonEventBus.add( ev -> {
			Object[] data = ev.getData();
			switch (ev.getType()) {
			case UI_MESSAGE:
				System.out.println("MESSAGE: "+data[1]);
				int type = (int) data[0];
				String mess = (String) data[1];
				Throwable cause = (data.length>2)?(Throwable) (data[2]):null;
				Path file = (data.length>3)?(Path)data[3]:null;
				showUIMessage(type, mess, cause, file);
				break;
			case OPEN_FILE:
				Path toOpen = (Path) data[0];
//				System.out.println("OPEN: "+toOpen);
//				System.out.println("OPEN2: "+Desktop.isDesktopSupported());
//				System.out.println("OPEN3: "+Toolkit.getDefaultToolkit());;
//				if (getHostServices() != null) {
//					logger.log(Level.INFO, "Open with HostServices "+getHostServices().getCodeBase());
//					getHostServices().showDocument(toOpen.toUri().toString());
//				} else
//				if (Desktop.isDesktopSupported()) {
//					logger.log(Level.INFO, "Open with AWT Desktop");
//					try {
//						Desktop.getDesktop().open(toOpen.toFile());
//					} catch (IOException e) {
//						logger.log(Level.ERROR, "Failed opening PDF: "+e);
//						e.printStackTrace();
//					}
//				} else {
					System.out.println("Open with Browser Service");
					logger.log(Level.INFO, "Open with Browser Service");
					openFile(toOpen);
//				}
				break;
			default:
				logger.log(Level.INFO, "Ignored "+ev);
			}
		});
    }

	//-------------------------------------------------------------------
    protected abstract <C extends RuleSpecificCharacterObject<?, ?, ?, ?>> C importXML(byte[] xml);

	//-------------------------------------------------------------------
    private void importFiles(List<File> files) {
		for (File file : files) {
			logger.log(Level.DEBUG, "File dropped: "+file);
			if (file.getName().toLowerCase().endsWith(".pdf") || file.getName().toLowerCase().endsWith(".xml")) {
				logger.log(Level.INFO, "Try importing from file: "+file);
				byte[] raw = null;
				try {
					if (file.getName().toLowerCase().endsWith(".xml")) {
						raw = Files.readAllBytes(file.toPath());
					} else {
						PdfReader reader = new PdfReader(new FileInputStream(file));
						@SuppressWarnings("unchecked")
						HashMap<String,String> meta = reader.getInfo();
						if (meta.containsKey("XML")) {
							raw = meta.get("XML").getBytes("UTF-8");
						} else {
							BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, ResourceI18N.get(RES, "import.pdf_without_xml"));
							return;
						}
					}
				} catch (IOException e) {
					logger.log(Level.ERROR, "Importing failed",e);
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, ResourceI18N.format(RES, "import.error", e.toString()));
					return;
				}

				try {
					logger.log(Level.INFO, "Import with XML data");
					RuleSpecificCharacterObject<?, ?, ?, ?> model = importXML(raw);
					if (model != null) {
						logger.log(Level.INFO, "Decoding of {0} successful", model.getName());
						CharacterProvider charProv = CharacterProviderLoader.getCharacterProvider();
						CharacterHandle handle = charProv.createCharacter(model.getName(), rules);
						handle.setCharacter(model);
						charProv.addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, model.getName()+".xml", raw);
						System.err.println("Short desc "+model.getShortDescription());
						handle.setShortDescription(model.getShortDescription());
						BabylonEventBus.fireEvent(BabylonEventType.CHAR_MODIFIED, 2);
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0,
								ResourceI18N.format(RES, "import.successful", model.getName()));

					} else {
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2,
								ResourceI18N.get(RES, "import.decoding_failed"));
					}
				} catch (IOException e) {
					e.printStackTrace();
					logger.log(Level.ERROR, "Importing failed",e);
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, ResourceI18N.format(RES, "import.error", e.toString()));
				}
			}
		}
    }

	//-------------------------------------------------------------------
    protected void configureDragAndDrop(Scene scene) {
    	scene.setOnDragOver(ev -> {
    		if (ev.getDragboard().getFiles()!=null && !ev.getDragboard().getFiles().isEmpty()) {
    			ev.acceptTransferModes(TransferMode.COPY_OR_MOVE);
    		}
    		ev.consume();
    	});
    	scene.setOnDragDropped(ev -> {
    		importFiles(ev.getDragboard().getFiles());
    		ev.setDropCompleted(true);
    		ev.consume();
    	});
    }

	//-------------------------------------------------------------------
    protected File[] getDirectories() {
		Optional<StorageService> storage = StorageService.create();
		File[] ret = new File[3];
		if (storage.isPresent()) {
			Optional<File> file = storage.get().getPrivateStorage();
			ret[0] = file.get();
			ret[1] = storage.get().getPublicStorage(rules.name().toLowerCase()).get();
		} else
			logger.log(Level.ERROR, "No StorageService found");
		ret[2] = new File(System.getProperty("user.home"));
		return ret;
    }

	//-------------------------------------------------------------------
	public void setEdenPingInfo(EdenPingInfo value) {
		edenInfo = value;
	}

	//-------------------------------------------------------------------
	public void setAccountInfo(EdenAccountInfo value) {
		logger.log(Level.INFO, "ENTER setAccountInfo");
		accountInfo = value;
		if (Platform.isFxApplicationThread()) {
			updateLabel();
		} else {
			Platform.runLater( () -> updateLabel());
		}
		logger.log(Level.INFO, "LEAVE setAccountInfo");
	}

	//-------------------------------------------------------------------
	private void updateLabel() {
		lbLoggedInUser.setText(accountInfo.getFirstName()+ " "+accountInfo.getLastName());
		Text text = new Text();
		if (accountInfo.isVerified()) {
			text.setText("\u2705");
			text.setFill(Color.LIGHTGREEN);
			lbLoggedInUser.setTooltip(new Tooltip(ResourceI18N.get(RES, "account.verified.true")));
		} else {
			text.setText("\u2753");
			text.setFill(Color.PALEVIOLETRED);
			lbLoggedInUser.setTooltip(new Tooltip(ResourceI18N.get(RES, "account.verified.false")));
		}
		lbLoggedInUser.setGraphic(text);
		logger.log(Level.INFO, "LEAVE updateLabel");
	}

	//-------------------------------------------------------------------
	public EdenAccountInfo getAccountInfo() {
		return accountInfo;
	}

	//-------------------------------------------------------------------
    public final boolean isOffline() {
    	return !eden.hasStateFlag(StateFlag.SERVER_ONLINE);
    }

	//-------------------------------------------------------------------
	/**
	 * @return TRUE when logging in should be tried, FALSE if no online account exists
	 */
	private boolean askAccountCreation() {
		AccountCreationDialog dialog = new AccountCreationDialog(eden, (l,p)->writeEdenCredentials(l,p));
		CloseType foo = EdenClientApplication.this.showAndWait(dialog);
		if (foo==CloseType.YES) {
			logger.log(Level.INFO, "User wants to create an account");
			EdenAccountInfo account = dialog.getAccountInfo();
			eden.login(dialog.getLogin(), dialog.getPassword(), null);

			VerificationCodeDialog vDialog = new VerificationCodeDialog(account.getEmail(), eden, 4);
			foo = EdenClientApplication.this.showAndWait(vDialog);
			if (foo==CloseType.APPLY) {
				logger.log(Level.INFO, "Entered code {0}", vDialog.getCode());
			}
			return true;
		} else {
			logger.log(Level.INFO, "User does not want create an account");
			return false;
		}
	}

	//-------------------------------------------------------------------
	protected void loadData() {
		logger.log(Level.INFO, "STEP: loadData");
		logger.log(Level.INFO, "I may load the following modules: "+String.join(",", allowedDatasets));
		if (allowedDatasets.length==0) {
			allowedDatasets = new String[] {"CORE.Light"};
			logger.log(Level.WARNING, "Allow only core rules light");
		}
	}

	//-------------------------------------------------------------------
	private void stepPages() {
		try {
			Page page = createPage(navigLookup);
			page.setAppLayout(getAppLayout());
			this.getAppLayout().navigateTo(page);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	protected void stepBlockingOnline() {
		logger.log(Level.INFO, "STEP: BlockingOnline with thread {0}", Thread.currentThread());

		(new CheckForUpdatesStep(this, appName)).run();
		(new PrepareEdenConnectionStep(this, appName, EDEN_SERVER, scheme)).run();
		(new CheckServerOnlineStep(this)).run();
		(new CheckAccountCreationStep(this)).run();
		(new LoginToServerStep(this)).run();


//		SettingsService service = Services.get(SettingsService.class).orElse(null);
//		if (service==null) {
//			logger.log(Level.ERROR, "No SettingService available");
//			return;
//		}
//
//		edenInfo = eden.getInfo();
//		logger.log(Level.INFO, "Received "+edenInfo);
//		if (isOffline()) {
//			// No connection to server
//			logger.log(Level.WARNING, "No connection to Eden server - Allow only registered modules");
//			allowedDatasets = readLicensesModules();
//			//allowedDatasets = new String[] {"CORE.Light"};
//			return;
//		}
//		logger.log(Level.INFO, "Eden server is online at "+edenInfo.getMainSiteURL());
//
//		// Check if user needs to be asked if (sh)he wants an Eden account
//		String[] pair = readEdenCredentials();
//		boolean needToAsk = (service.retrieve(CFG_ALREADY_ASKED_ACCOUNT)==null) || !Boolean.parseBoolean( service.retrieve(CFG_ALREADY_ASKED_ACCOUNT) );
//		logger.log(Level.INFO, "Need to ask = "+needToAsk);
//		if (needToAsk) {
//			// No account given and not asked yet
//			askAccountCreation();
//			service.store(CFG_ALREADY_ASKED_ACCOUNT, "true");
//		}
//
//
//		/*
//		 * See if there already is a user information present
//		 */
//		try {
//			logger.log(Level.INFO, "1");
//			accountInfo = eden.getAccountInfo();
//			logger.log(Level.INFO, "2");
//		} catch (EdenAPIException e) {
//			logger.log(Level.WARNING, "Error getting account info from server", e);
//			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Error talking to Eden Server",e);
//		}
//		if (accountInfo!=null) {
//			logger.log(Level.INFO, "Started by "+accountInfo.getFirstName()+" "+accountInfo.getLastName());
//			logger.log(Level.INFO, " Email is "+accountInfo.getEmail());
//			logger.log(Level.INFO, " Licensened: {0}",accountInfo.getLicensedModules());
//			allowedDatasets = writeLicensedModules(accountInfo.getLicensedModules().get(rules));
//		} else {
////			if (authenticationCancelled) {
////				logger.log(Level.INFO, "Authentication cancelled");
////			} else {
//				logger.log(Level.WARNING, "Eden Server is online, but account ''{0}'' does not exist", pair[0]);
//				allowedDatasets = readLicensesModules();
//				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1,
//						ResourceI18N.format(RES, "error.login_failed", pair[0]));
////			}
//		}

		// Synchronize characters
		stepSynchronize();
	}

	//-------------------------------------------------------------------
	protected void openFile(Path path) {
		BrowserService.create().ifPresent( browser -> {
			try {
				if (path.toString().startsWith("/storage/emulated/0/")) {
					browser.launchExternalBrowser("file://"+path.toString().substring("/storage/emulated/0/".length()));

				} else {
					browser.launchExternalBrowser("file:"+path.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.ERROR, "Error opening {0}", path);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void setPDFEnable(boolean value) {
		logger.log(Level.INFO, "Set PDF feature to {0}", value);
		String key = "pdf."+rules+".enable";
		SettingsService service = Services.get(SettingsService.class).get();
		if (service!=null) {
				service.store(key, String.valueOf(value));
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			pref.put(key, String.valueOf(value));
		}

		PDFViewerConfig.setEnabled(value);
	}

	//-------------------------------------------------------------------
	protected boolean isPDFEnabled() {
		String key = "pdf."+rules+".enable";
		if (!Services.get(SettingsService.class).isPresent()) {
			logger.log(Level.ERROR, key);
			return false;
		}
		SettingsService service = Services.get(SettingsService.class).get();
		if (service!=null) {
			return  (service.retrieve(key)!=null)?Boolean.parseBoolean(service.retrieve(key)):false;
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			return Boolean.parseBoolean(pref.get(key, "false"));
		}
	}

	//-------------------------------------------------------------------
	protected void setPDFPathFor(RoleplayingSystem rules, String id, String lang, String path, int offset) {
		String key = "pdf."+rules+"."+id+"."+lang;
		String offsetKey = "pdf."+rules+"."+id+"."+lang+".offset";
		SettingsService service = Services.get(SettingsService.class).get();
		if (service!=null) {
			if (path==null) {
				service.remove(key);
				service.remove(offsetKey);
			} else {
				service.store(key, path);
				service.store(offsetKey, String.valueOf(offset));
			}
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			if (path==null) {
				pref.remove(key);
				pref.remove(offsetKey);
			} else {
				pref.put(key, path);
				pref.put(offsetKey, String.valueOf(offset));
			}
		}
	}

	//-------------------------------------------------------------------
	protected PathAndOffset getPDFPathFor(RoleplayingSystem rules, String id, String lang) {
		String key = "pdf."+rules+"."+id+"."+lang;
		String offsetKey = "pdf."+rules+"."+id+"."+lang+".offset";
		Optional<SettingsService> service = Services.get(SettingsService.class);
		if (service.isPresent()) {
			int offset = (service.get().retrieve(offsetKey)!=null)?Integer.parseInt( service.get().retrieve(offsetKey) ):0;
			String path = (service.get().retrieve(key)!=null)?service.get().retrieve(key):"";
			return new PathAndOffset(path, offset);
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			return  new PathAndOffset(pref.get(key, null), Integer.parseInt(pref.get(offsetKey, "0")));
		}
	}

	//-------------------------------------------------------------------
	protected int getPDFOffsetFor(RoleplayingSystem rules, String id, String lang) {
		String key = "pdf."+rules+"."+id+"."+lang+".offset";
		Optional<SettingsService> service = Services.get(SettingsService.class);
		if (service.isPresent()) {
			return (service.get().retrieve(key)!=null)?Integer.parseInt( service.get().retrieve(key) ):0;
		} else {
			Preferences pref = Preferences.userNodeForPackage(EdenClientApplication.class);
			return Integer.parseInt(pref.get(key, "0"));
		}
	}
	//-------------------------------------------------------------------
	public void handleError(CharacterIOException e) {
		String mess = "An error occurred: "+e;
		switch (e.getCode()) {
		case FILESYSTEM_WRITE:
			mess = ResourceI18N.format(RES, "error.chario.fswrite", e.getMessage());
		}

		Label lbValue = new Label("Value: "+e.getValue());
		lbValue.setWrapText(true);
		Label lbPath = new Label("Path: "+e.getPath());
		lbPath.setWrapText(true);
		Label lbMess = new Label(e.getMessage());
		lbMess.setWrapText(true);
		VBox box = new VBox(5, lbValue, lbPath, lbMess);
		try {
			throw new RuntimeException("Trace");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		showAlertAndCall(AlertType.ERROR, e.getCode().name(), box);
		System.exit(1);
	}

	//-------------------------------------------------------------------
	public void handleError(Throwable e) {
		Label lbMess = new Label(e.getMessage());
		lbMess.setWrapText(true);
		showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), lbMess);
		System.exit(1);
	}

	//-------------------------------------------------------------------
	protected void checkForUpdates() {
		logger.log(Level.INFO, "ENTER checkForUpdates");
		String installed = System.getProperty("project.version", "99.99.99");
		try {
			URL updateURL = new URL("http://updates.rpgframework.de/"+appName.toLowerCase()+"-updates/version.txt");
			logger.log(Level.INFO, "Update check at "+updateURL);
			HttpURLConnection con = (HttpURLConnection) updateURL.openConnection();
			con.setRequestProperty("User-Agent", String.format("%s / %s", installed, System.getProperty("os.name")));
			int code = con.getResponseCode();
			if (code==200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.defaultCharset()));
				Stream<String> stream = in.lines();
				Iterator<String> lines = stream.collect(Collectors.toList()).iterator();
				String version = lines.next();
				String dateRaw = lines.next();
				String changelogURL = lines.hasNext()?lines.next():null;
				String downloadURL = lines.hasNext()?lines.next():"https://www.rpgframework.de/commlink6/download";

				boolean newerAvailable = isNewerVersionAvailable(installed, version);
				if (newerAvailable) {
					LocalDate date = LocalDate.parse(dateRaw,new DateTimeFormatterBuilder()
						    .appendPattern("yyyy-MM-dd")
						    .parseDefaulting(ChronoField.NANO_OF_DAY, 0)
						    .toFormatter()
						    .withZone(ZoneId.of("Europe/Berlin")));
					//(new SimpleDateFormat("YYYY-MM-DD")).parse(date)
					showUpdateMessage(installed, version, date, changelogURL, downloadURL);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			logger.log(Level.INFO, "LEAVE checkForUpdates");
		}

	}

	//-------------------------------------------------------------------
	private static boolean isNewerVersionAvailable(String installed, String available) {
		if (available==null)
			return false;

		if (installed.equals(available)) {
			logger.log(Level.INFO,"Version is up to date");
			return false;
		}

		StringTokenizer tokInst = new StringTokenizer(installed, ". -");
		StringTokenizer tokAvai = new StringTokenizer(available, ". -");

		try {
			for (int i=0; i<3; i++) {
				if (!tokInst.hasMoreTokens() || !tokAvai.hasMoreTokens())
					break;
				Integer valI = Integer.parseInt(tokInst.nextToken());
				Integer valA = Integer.parseInt(tokAvai.nextToken());
				if (valA>valI) {
					logger.log(Level.INFO,"'"+available+"' is newer than '"+installed+"'");
					return true;
				} else if (valA<valI) {
					logger.log(Level.INFO,"'"+available+"' is older than '"+installed+"'");
					return false;
				}
			}
		} catch (NumberFormatException e) {
			logger.log(Level.WARNING,"Failed parsing versions: installed="+installed+"  remote="+available+"  : "+e);
			return false;
		} catch (NoSuchElementException e) {
			logger.log(Level.WARNING,"Failed parsing versions: installed="+installed+"  remote="+available+"  : "+e);
			return true;
		}

		logger.log(Level.WARNING,"Assuming that '"+available+"' is newer than '"+installed+"'");
		return true;
	}

	//-------------------------------------------------------------------
	private void stepSynchronize() {
		logger.log(Level.INFO, "STEP synchronize");
		SyncingCharacterProvider<CharacterHandle> prov = (SyncingCharacterProvider<CharacterHandle>) CharacterProviderLoader.getCharacterProvider();
		String[] pair = readEdenCredentials();
		if (prov!=null && accountInfo!=null) {
//			eden.login(pair[0], pair[1]);
//			logger.log(Level.INFO, "setAccountData-------------------------------------");
			prov.start();
		}

	}

	//-------------------------------------------------------------------
	protected void logout() {
		logger.log(Level.INFO, "ENTER logout");

		Optional<SettingsService> opt =  Services.get(SettingsService.class);
		if (opt.isPresent()) {
			SettingsService service = Services.get(SettingsService.class).get();
			service.remove(PREF_USER);
			service.remove(PREF_PASS);
			service.remove("eden.askedAccount");
			eden.logout();
		} else {
			logger.log(Level.WARNING, "Cannot log out, because I haven't been logged in");
		}
		accountInfo = null;
		logger.log(Level.INFO, "LEAVE logout");
	}

	//-------------------------------------------------------------------
	public void deleteAccount() {
		logger.log(Level.INFO, "ENTER deleteAccount");
		try {
			eden.deleteAccount();
			accountInfo = null;
			writeEdenCredentials(null, null);
		} catch (EdenAPIException e) {
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Deletion failed", e);
		} finally {
			logger.log(Level.INFO, "LEAVE deleteAccount");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Return the URL to a page that links license shops
	 * @return
	 */
	public String getLicenseURL() {
		return licenseURL;
	}

	//-------------------------------------------------------------------
	public void accept(List<StateFlag> flags) {
		logger.log(Level.WARNING, "Connection state changed to {0}", flags);
//		// Check if server is online
//		if (!flags.contains(StateFlag.SERVER_ONLINE)) {
//			logger.log(Level.INFO, "Check if server is online");
//			CheckServerOnlineStep step = new CheckServerOnlineStep(this);
//			if (step.canRun()) {
//				step.run();
//			}
//			return;
//		}
//		Platform.runLater( () -> checkAccountToDo());
	}

//	//-------------------------------------------------------------------
//	private void checkAccountToDo() {
//		if (!Platform.isFxApplicationThread()) {
//			throw new IllegalStateException("Must be called from FX thread");
//		}
//
////		List<StateFlag> flags = eden.getStateFlags();
////		if (!flags.contains(StateFlag.CREDENTIALS_EXIST)) {
////			logger.log(Level.INFO, "Check if credentials exist");
////			CheckAccountCreationStep step = new CheckAccountCreationStep(this);
////			if (step.canRun()) {
////				step.run();
////			}
////			return;
////		}
//	}

}