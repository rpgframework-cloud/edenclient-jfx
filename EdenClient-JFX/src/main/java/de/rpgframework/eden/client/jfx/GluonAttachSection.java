package de.rpgframework.eden.client.jfx;

import java.io.File;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.util.Optional;
import java.util.ResourceBundle;

import org.prelle.javafx.Section;
import org.prelle.javafx.TitledComponent;
import org.prelle.javafx.layout.FlexGridPane;

import com.gluonhq.attach.browser.BrowserService;
import com.gluonhq.attach.device.DeviceService;
import com.gluonhq.attach.display.DisplayService;
import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.storage.StorageService;

import de.rpgframework.ResourceI18N;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class GluonAttachSection extends Section {

	private static ResourceBundle RES = ResourceBundle.getBundle(EdenClientApplication.class.getName(), EdenClientApplication.class.getModule());

	private Label lbBrowser;
	private Label lbDevice;
	private Label lbDisplay;
	private Label lbPicture;
	private Label lbSetting;
	private Label lbStorage;

	private VBox bxBrowser, bxDevice, bxDisplay, bxPicture, bxSetting, bxStorage;

	//-------------------------------------------------------------------
	public GluonAttachSection() {
		super(ResourceI18N.get(RES, "section.gluonattach"), null);
		initComponents();
		initLayout();
		setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(this, 5);
		FlexGridPane.setMinHeight(this, 8);
		FlexGridPane.setMediumWidth(this, 8);
		FlexGridPane.setMediumHeight(this, 6);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbBrowser = new Label("?"); lbBrowser.setAlignment(Pos.CENTER_RIGHT);
		lbDevice  = new Label("?"); lbDevice.setAlignment(Pos.CENTER_RIGHT);
		lbDisplay = new Label("?"); lbDisplay.setAlignment(Pos.CENTER_RIGHT);
		lbPicture = new Label("?"); lbPicture.setAlignment(Pos.CENTER_RIGHT);
		lbSetting = new Label("?"); lbSetting.setAlignment(Pos.CENTER_RIGHT);
		lbStorage = new Label("?"); lbStorage.setAlignment(Pos.CENTER_RIGHT);

		bxBrowser= new VBox(0);
		bxDevice = new VBox(0);
		bxDisplay= new VBox(0);
		bxPicture= new VBox(0);
		bxSetting= new VBox(0);
		bxStorage= new VBox(0);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdBrowser = new Label("BrowserService");
		Label hdDevice  = new Label("DeviceService");
		Label hdDisplay = new Label("DisplayService");
		Label hdPicture = new Label("PictureService");
		Label hdSetting = new Label("SettingsService");
		Label hdStorage = new Label("StorageService");

		GridPane grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(10);
		int y = add(grid, 0, lbBrowser, hdBrowser, bxBrowser);
		y = add(grid, y, lbDevice, hdDevice, bxDevice);
		y = add(grid, y, lbDisplay, hdDisplay, bxDisplay);
		y = add(grid, y, lbPicture, hdPicture, bxPicture);
		y = add(grid, y, lbSetting, hdSetting, bxSetting);
		y = add(grid, y, lbStorage, hdStorage, bxStorage);

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setFitToWidth(true);
		setContent(scroll);
	}

	//-------------------------------------------------------------------
	private int add(GridPane grid, int y, Label lb, Label hd, VBox content) {
		grid.add(hd, 0, y);
		grid.add(lb, 1, y);
		GridPane.setMargin(lb, new Insets(5, 0, 0, 0));
		GridPane.setMargin(hd, new Insets(5, 0, 0, 0));
			grid.add(content, 0, y+1, 2,1);
		return y+2;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		bxBrowser.getChildren().clear();
		if (BrowserService.create().isPresent()) {
			lbBrowser.setText("YES"); lbBrowser.setStyle("-fx-text-fill: green");
		} else {
			lbBrowser.setText("NO"); lbBrowser.setStyle("-fx-text-fill: red");
		}

		bxDevice.getChildren().clear();
		Optional<DeviceService> device = DeviceService.create();
		if (device.isPresent()) {
			lbDevice.setText("YES"); lbDevice.setStyle("-fx-text-fill: green");
			bxDevice.getChildren().add( new TitledComponent("  Model", new Label( device.get().getModel() )) );
			bxDevice.getChildren().add( new TitledComponent("  Platform", new Label( device.get().getPlatform() )) );
			bxDevice.getChildren().add( new TitledComponent("  Version", new Label( device.get().getVersion() )) );
			bxDevice.getChildren().add( new TitledComponent("  UUID", new Label( device.get().getUuid() )) );
		} else {
			lbDevice.setText("NO"); lbDevice.setStyle("-fx-text-fill: red");
		}

		bxDisplay.getChildren().clear();
		Optional<DisplayService> display = DisplayService.create();
		if (display.isPresent()) {
			lbDisplay.setText("YES"); lbDisplay.setStyle("-fx-text-fill: green");
			bxDisplay.getChildren().add( new TitledComponent("  Default Dimension", new Label( display.get().getDefaultDimensions().getWidth()+"x"+display.get().getDefaultDimensions().getHeight() )) );
			bxDisplay.getChildren().add( new TitledComponent("  Screen Resolution", new Label( display.get().getScreenResolution().getWidth()+"x"+display.get().getScreenResolution().getHeight() )) );
			bxDisplay.getChildren().add( new TitledComponent("  Screen Scale", new Label( display.get().getScreenScale()+"" )) );
			bxDisplay.getChildren().add( new TitledComponent("  Is Phone", new Label( display.get().isPhone()+"" )) );
			bxDisplay.getChildren().add( new TitledComponent("  Is Tablet", new Label( display.get().isTablet()+"" )) );
			bxDisplay.getChildren().add( new TitledComponent("  Is Desktop", new Label( display.get().isDesktop()+"" )) );
			bxDisplay.getChildren().add( new TitledComponent("  Has Notch", new Label( display.get().hasNotch()+"" )) );
		} else {
			lbDisplay.setText("NO"); lbDisplay.setStyle("-fx-text-fill: red");
		}

//		if (PictureService.create().isPresent()) {
//			lbDisplay.setText("YES"); lbDisplay.setStyle("-fx-text-fill: green");
//		} else {
//			lbDisplay.setText("NO"); lbDisplay.setStyle("-fx-text-fill: red");
//		}

		if (SettingsService.create().isPresent()) {
			lbSetting.setText("YES"); lbSetting.setStyle("-fx-text-fill: green");
		} else {
			lbSetting.setText("NO"); lbSetting.setStyle("-fx-text-fill: red");
		}

		bxStorage.getChildren().clear();
		Optional<StorageService> storage = StorageService.create();
		if (storage.isPresent()) {
			lbStorage.setText("YES"); lbStorage.setStyle("-fx-text-fill: green");
			bxStorage.getChildren().add( new TitledComponent("  Private", new Label( storage.get().getPrivateStorage().isPresent()?(storage.get().getPrivateStorage().get()+""):"Unavailable" )) );
			bxStorage.getChildren().add( new TitledComponent("  Public", new Label( storage.get().getPublicStorage("/").isPresent()?(storage.get().getPublicStorage("/").get()+""):"Unavailable" )) );
		} else {
			lbStorage.setText("NO"); lbStorage.setStyle("-fx-text-fill: red");
		}
	}

}
