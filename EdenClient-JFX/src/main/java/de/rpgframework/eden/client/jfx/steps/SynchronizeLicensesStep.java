package de.rpgframework.eden.client.jfx.steps;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.ResourceBundle;

import com.gluonhq.attach.settings.SettingsService;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.OnlineStartupStep;
import de.rpgframework.eden.client.jfx.LoginDialog;
import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.reality.CryptoDecoder;

/**
 * @author prelle
 *
 */
public class SynchronizeLicensesStep implements OnlineStartupStep, EdenClientConstants {

	private final static ResourceBundle RES = ResourceBundle.getBundle(LoginDialog.class.getPackageName()+".Dialogs");

	protected static Logger logger = System	.getLogger(SynchronizeLicensesStep.class.getPackageName());
	private final static String LICENSE_KEY = "licenses";

	private EdenConnection eden;
	private RoleplayingSystem rules;

	//-------------------------------------------------------------------
	public SynchronizeLicensesStep(RoleplayingSystem rules) {
		this.rules = rules;
	}

	//-------------------------------------------------------------------
	private static List<String> decode(String encoded) {
		List<String> ret = new java.util.ArrayList<>();
		if (encoded!=null) {
			String plain = CryptoDecoder.decode(encoded);
			for (String line : plain.split("\n")) {
				line = line.trim();
				if (!line.isBlank()) {
					ret.add(line);
				}
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			if (eden==null || eden.isOffline()) {
				logger.log(Level.WARNING, "Offline - don't synchronize");
				SettingsService.create().ifPresent(service -> {
					String value = service.retrieve(LICENSE_KEY);
					// Try to decode a local license key copy if there is one
					if (value!=null) {
						List<String> datasets = decode(value);
						logger.log(Level.INFO, "Offline - found {0} datasets configured locally", datasets.size());
						LicenseManager.storeGlobalLicenses(datasets);
						logger.log(Level.INFO, "Local datasets: {0}", datasets);
					}
				});
				return;
			}

			String encoded = eden.getShopClient().getLicensedDatasets(rules);
			SettingsService.create().ifPresent(service -> {
				service.store(LICENSE_KEY, encoded);
			});

			List<String> datasets = decode(encoded);
			logger.log(Level.INFO, "Synchronized datasets: {0}", datasets);
			LicenseManager.storeGlobalLicenses(datasets);
		} catch (Throwable ioe) {
			logger.log(Level.ERROR, "Failed to synchronize licenses",ioe);
			String message = ResourceI18N.get(RES, "license.read.ioerror");
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, message, ioe);
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return true;
//		return parent.getEdenConnection()!=null && !parent.isOffline() && parent.getAccountInfo()!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#updateConnection(de.rpgframework.eden.client.EdenConnection)
	 */
	@Override
	public void updateConnection(EdenConnection con) {
		this.eden = con;
	}

}
