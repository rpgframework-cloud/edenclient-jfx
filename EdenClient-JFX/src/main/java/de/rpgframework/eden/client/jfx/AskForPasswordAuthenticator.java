package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;

import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.util.Services;

import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.InteractiveAuthenticator;
import javafx.application.Platform;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class AskForPasswordAuthenticator implements InteractiveAuthenticator {

	protected static Logger logger = System	.getLogger(EdenClientApplication.class.getPackageName());

	private Supplier<EdenConnection> supplier;
	private BiConsumer<String, String> credentialsWriter;
	private Image image;

	protected boolean authenticationCancelled;

	//-------------------------------------------------------------------
	public AskForPasswordAuthenticator(Supplier<EdenConnection> supplier, BiConsumer<String, String> credentialsWriter, Image image) {
		this.supplier = supplier;
		this.credentialsWriter = credentialsWriter;
		this.image  = image;
	}

	//-------------------------------------------------------------------
	private EdenConnection getEden() { return supplier.get(); }

	//-------------------------------------------------------------------
	/**
	 * Read user credentials from SettingService - if present - or from
	 * Preferences otherwise
	 */
	private String[] readEdenCredentials() {
		SettingsService service = Services.get(SettingsService.class).orElse(null);
		if (service!=null) {
			String user = service.retrieve(EdenClientApplication.PREF_USER);
			String pass = service.retrieve(EdenClientApplication.PREF_PASS);
			return new String[] {user,pass};
		}
		return new String[2];
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.client.InteractiveAuthenticator#requestCredentials()
	 */
	@Override
	public String[] requestCredentials() {
		logger.log(Level.INFO, "requestCredentials "+Thread.currentThread());

		LoginDialog dialog = new LoginDialog(0, getEden());
		String[] pair = readEdenCredentials();
		dialog.setLogin(pair[0]);
		dialog.setPassword(pair[1]);
		dialog.setImage(image);

		Runnable run = new Runnable() {
			public void run() {
				 logger.log(Level.WARNING, "Call showAndWait");
				 dialog.setVisible(true);
				 Object foo = FlexibleApplication.getInstance().showAndWait(dialog);
				 logger.log(Level.WARNING, "Returned "+foo);
				 if (foo!=CloseType.OK) {
					 authenticationCancelled = true;
					 logger.log(Level.WARNING, "Authentication to Eden server cancelled by user");
				 } else {
					 authenticationCancelled = false;
				 }
				 credentialsWriter.accept(dialog.getLogin(), dialog.getPassword());
				 logger.log(Level.INFO, "Change user from {0} to {1}", pair[0], dialog.getLogin());
				 logger.log(Level.INFO, "Change pass from {0} to {1}", pair[1], dialog.getPassword());
				 synchronized (getEden()) {
					 getEden().notify();
				 }
			}
		};
		logger.log(Level.WARNING, "isFxApplicationThread "+Platform.isFxApplicationThread());
		if (Platform.isFxApplicationThread()) {
			logger.log(Level.WARNING, "in FxApplicationThread");
			run.run();
			if (authenticationCancelled) {
				logger.log(Level.INFO, "LEAVE requestCredentials with CANCEL");
				return null;
			}
		} else {
				logger.log(Level.WARNING, "in synchronized");
				Platform.runLater(run);
				try {
					logger.log(Level.WARNING, "call wait");
					synchronized (getEden()) {
						getEden().wait(30000);
					}
					if (authenticationCancelled)
						return null;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		String lastUser = dialog.getLogin();
		String lastPass = dialog.getPassword();
		logger.log(Level.INFO, "LEAVE requestCredentials with OK");
		return new String[] {lastUser, lastPass};
	}

}
