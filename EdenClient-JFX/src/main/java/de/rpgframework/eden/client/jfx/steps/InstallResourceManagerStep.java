package de.rpgframework.eden.client.jfx.steps;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;

import de.rpgframework.character.CharacterIOException;
import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.RemoteAndLocalCustomDataSetManager;
import de.rpgframework.eden.client.RemoteAndLocalCustomResourceManager;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.EdenSettings;
import de.rpgframework.genericrpg.data.CustomDataSetManagerLoader;

/**
 * @author prelle
 *
 */
public class InstallResourceManagerStep implements StartupStep {

	protected static Logger logger = System	.getLogger(InstallResourceManagerStep.class.getPackageName());

	private EdenClientApplication parent;
	private RoleplayingSystem rules;

	//-------------------------------------------------------------------
	public InstallResourceManagerStep(EdenClientApplication parent, RoleplayingSystem rules) {
		this.parent = parent;
		this.rules  = rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return parent.getEdenConnection()!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		/*
		 * 1. Determine local data directory
		 */
		Path customDir = EdenSettings.appDir.resolve("custom");
		logger.log(Level.INFO, "Use "+customDir+" as custom data directory");
		try {
			if (!Files.exists(customDir))
				Files.createDirectories(customDir);
		} catch (IOException e1) {
			logger.log(Level.ERROR, "Failed creating custom data dir: ",e1);
		}
		System.setProperty("eden.customdir", customDir.toAbsolutePath().toString());

		/*
		 * 2. Prepare resource manager
		 */
		RemoteAndLocalCustomResourceManager resMan = null;
		try {
			resMan = new RemoteAndLocalCustomResourceManager(customDir, parent.getEdenConnection(), rules);
			CustomResourceManagerLoader.setResourceManager(resMan);

			Path defaultLoc = customDir.resolve("default").resolve("i18n").resolve("default.properties");
			if (!Files.exists(defaultLoc.getParent())) {
				logger.log(Level.INFO, "Create directory {0}", defaultLoc.getParent());
				Files.createDirectories(defaultLoc.getParent());
			}
			if (!Files.exists(defaultLoc)) {
				logger.log(Level.INFO, "Create file {0}", defaultLoc);
				Files.createFile(defaultLoc);
			}
			CustomDataSetManagerLoader.setResourceManager(new RemoteAndLocalCustomDataSetManager(customDir, parent.getEdenConnection(), rules));

		} catch (CharacterIOException e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Error setting up resource manager",e);
			parent.handleError(e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Unknown Error setting up resource manager",e);
			parent.handleError(e);
		}

		/*
		 * 2. Prepare custom data manager
		 */
		RemoteAndLocalCustomDataSetManager dataMan = null;
		try {
			dataMan = new RemoteAndLocalCustomDataSetManager(customDir, parent.getEdenConnection(), rules);
			CustomDataSetManagerLoader.setResourceManager(dataMan);
		} catch (CharacterIOException e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Error setting up custom data manager",e);
			parent.handleError(e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Unknown Error setting up custom data manager",e);
			parent.handleError(e);
		}
	}

}
