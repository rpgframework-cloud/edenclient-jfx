package de.rpgframework.eden.client.jfx;

import java.io.File;
import java.lang.System.Logger;
import java.nio.file.FileSystems;

import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Window;

/**
 * @author prelle
 *
 */
public class EdenDebugPage extends Page {

	protected final static Logger logger = JavaFXConstants.logger;
	
	private Label label4;
	private ChangeListener<Number> listener;
	
	private TextArea taEvents;
	private ObservableList<String> events;
	private FlexGridPane flex;
	
	private GluonAttachSection secGluon;
	
	private File[] directories;

	//-------------------------------------------------------------------
	public EdenDebugPage(File[] directories) {
		super("Debug");
		this.setId("debug");
		this.directories = directories;
		
		secGluon = new GluonAttachSection();
		
		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secGluon);
		setContent(flex);
		events = FXCollections.observableArrayList();
		taEvents = new TextArea();
		taEvents.setPrefColumnCount(20);
		taEvents.setPrefRowCount(6);
		taEvents.setEditable(false);
		

		this.sceneProperty().addListener( (ov,o,n) -> {
			System.err.println("DebugPage: scene = "+n);
			if (n!=null)
				initContent();
		});
		initContent();
		initInteractivity();
		
		listener = ( (ov,o,n) -> {
	        Window stage = this.getScene().getWindow();
	        label4 .setText("Stage = "+stage.getWidth()+"x"+stage.getHeight());
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @param title
	 */
	private void initContent() {
       String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        label.setWrapText(true);
        Label label1 = new Label("OS is "+System.getProperty("os.name"));
        label1.setWrapText(true);
        Label label2 = new Label("User Home "+FileSystems.getDefault().getRootDirectories().iterator().next()+" = "+directories[2]);
        label2.setWrapText(true);
        if (directories[2]!=null) label2.setText(label2.getText()+" : "+directories[2].canWrite());
        Label label2b = new Label("Priv. Storage "+directories[0]);
        label2b.setWrapText(true);
        if (directories[0]!=null) label2b.setText(label2b.getText()+" : "+directories[0].canWrite());
        Label label2c = new Label("Publ. Storage "+directories[1]);
        label2c.setWrapText(true);
        if (directories[1]!=null) label2c.setText(label2c.getText()+" : "+directories[1].canWrite());
        Label label3 = new Label("Screen = "+Screen.getPrimary().getVisualBounds().getWidth()+"x"+Screen.getPrimary().getVisualBounds().getHeight()+"  scale="+Screen.getPrimary().getOutputScaleX());
        label3.setWrapText(true);
        if (this.getScene()!=null) {
            Window stage = this.getScene().getWindow();
        	label4 = new Label("Stage = "+stage.getWidth()+"x"+stage.getHeight());
        } else if (getParent()!=null) {
           	label4 = new Label("Stage: not set and parent has  "+getParent().getScene());
       } else {
        	label4 = new Label("Stage: not set  ");
        }
        label4.setWrapText(true);
        VBox infos = new VBox(5,label,label1,label2,label2b, label2c, label3, label4);
        infos.getStyleClass().add("section");
        infos.setStyle("-fx-max-width: 22em; -fx-pref-width: 22em");
        infos.setId("section-debug-infos");
	}

	//-------------------------------------------------------------------
	public void refresh() {
		initContent();
	}

	//-------------------------------------------------------------------
	private void addEvent(String text) {
		events.add(0, text);
		if (events.size()>5)
			events.remove(5);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		setOnEnterAction(ev -> {
			if (getScene()!=null)
			getScene().widthProperty().addListener(listener);
		});
		setOnLeaveAction(ev -> {
			if (getScene()!=null)
			getScene().widthProperty().removeListener(listener);
		});
		events.addListener(new ListChangeListener<String>() {
			@Override
			public void onChanged(Change<? extends String> c) {
				taEvents.setText(String.join("\n", events));
			}});
		
//		flow.setOnSwipeLeft (ev -> addEvent("Swipe Left"));
//		flow.setOnSwipeRight(ev -> addEvent("Swipe Right"));
//		flow.setOnSwipeDown (ev -> addEvent("Swipe Down"));
//		flow.setOnSwipeUp   (ev -> addEvent("Swipe Up"));
//		flow.setOnKeyPressed(ev -> addEvent("Key Pressed: "+ev.getCode()));
		FlexibleApplication.getInstance().getAppLayout().getScene().setOnKeyPressed(ev -> addEvent("Key Pressed2: "+ev));
	}

}
