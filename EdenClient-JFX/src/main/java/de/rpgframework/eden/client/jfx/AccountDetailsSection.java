package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.Section;

import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.util.Services;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenClientConstants;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 *
 */
public class AccountDetailsSection extends Section implements EdenClientConstants {

	private final static ResourceBundle RES = ResourceBundle.getBundle(AccountDetailsSection.class.getPackageName()+".Sections");

	protected static Logger logger = System	.getLogger(AccountDetailsSection.class.getPackageName());

	private AccountDetailsPane pane;
	private Label lbAccountInfo;
	private Button btnLogout;
	private Button btnDelete;
	private Button btnCreate;
	private Button btnUpdate;
	private transient EdenClientApplication app;

	//-------------------------------------------------------------------
	public AccountDetailsSection(EdenClientApplication app) {
		super(ResourceI18N.get(RES, "section.accountdetails.title"), null);
		this.app = app;
		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		pane = new AccountDetailsPane();

		btnLogout = new Button(ResourceI18N.get(RES,"section.accountdetails.logout.button"));
		btnDelete = new Button(ResourceI18N.get(RES,"section.accountdetails.delete.button"));
		btnDelete.setStyle("-fx-background-color: highlight; -fx-text-fill: white;");
		btnUpdate = new Button(ResourceI18N.get(RES,"section.accountdetails.update.button"));

		lbAccountInfo = new Label(ResourceI18N.get(AccountCreationDialog.RES, "account.intro"));
		lbAccountInfo.setWrapText(true);
		btnCreate = new Button(ResourceI18N.get(RES,"section.accountdetails.create.button"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		btnLogout.setMaxWidth(Double.MAX_VALUE);
		btnDelete.setMaxWidth(Double.MAX_VALUE);
		btnUpdate.setMaxWidth(Double.MAX_VALUE);
		btnUpdate.setMaxWidth(Double.MAX_VALUE);
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		TilePane buttons = new TilePane(20,10, btnLogout, btnDelete, btnCreate, buf, btnUpdate);

		VBox layout = new VBox(20, lbAccountInfo, pane, buttons);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnLogout.setOnAction(e -> {
			CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(
					AlertType.CONFIRMATION,
					ResourceI18N.get(RES,"section.accountdetails.logout.title"),
					ResourceI18N.get(RES,"section.accountdetails.logout.text"));
			if (closed==CloseType.YES || closed==CloseType.OK) {
				logger.log(Level.INFO, "Logging out");
				app.logout();
				pane.clear();
				logger.log(Level.INFO, "Refresh");
				refresh();
			}
		});
		btnDelete.setOnAction(e -> {
			logger.log(Level.WARNING, "Delete account clicked");
			CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(
					AlertType.CONFIRMATION,
					ResourceI18N.get(RES,"section.accountdetails.delete.title"),
					ResourceI18N.get(RES,"section.accountdetails.delete.text"));
			if (closed==CloseType.YES || closed==CloseType.OK) {
				app.deleteAccount();
				pane.clear();
				refresh();
			}
		});
		btnCreate.setOnAction(e -> {
			askAccountCreation();
			refresh();
		});
		btnUpdate.setOnAction(e -> {
			showAccountUpdateDialog();
			refresh();
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		EdenAccountInfo info = app.getAccountInfo();
		pane.setData(info);
		if (info==null || info.getLogin()==null || info.getLogin().isEmpty()) {
			// No account yet
			btnLogout.setVisible(false);
			btnLogout.setManaged(false);
			btnDelete.setVisible(false);
			btnDelete.setManaged(false);
			btnUpdate.setVisible(false);
			btnUpdate.setManaged(false);
			pane.setVisible(false);
			pane.setManaged(false);

			btnCreate.setVisible(true);
			btnCreate.setManaged(true);
			lbAccountInfo.setVisible(true);
			lbAccountInfo.setManaged(true);
		} else {
			btnLogout.setVisible(true);
			btnLogout.setManaged(true);
			btnDelete.setVisible(true);
			btnDelete.setManaged(true);
			btnUpdate.setVisible(true);
			btnUpdate.setManaged(true);
			pane.setVisible(true);
			pane.setManaged(true);

			btnCreate.setVisible(false);
			btnCreate.setManaged(false);
			lbAccountInfo.setVisible(false);
			lbAccountInfo.setManaged(false);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return TRUE when logging in should be tried, FALSE if no online account exists
	 */
	private boolean askAccountCreation() {
		AccountCreationDialog dialog = new AccountCreationDialog(app.getEdenConnection(), (l,p)->writeEdenCredentials(l,p));
		CloseType foo = app.showAndWait(dialog);
//		CloseType foo = showDialog(dialog);
		AskForPasswordAuthenticator interactiveAuth = new AskForPasswordAuthenticator(
				() -> app.getEdenConnection(),
				(l,p)->writeEdenCredentials(l,p),
				app.getSecurityDialogImage()
			);
		logger.log(Level.INFO, "Thread "+Thread.currentThread()+"  isFX="+Platform.isFxApplicationThread());
		if (foo==CloseType.YES) {
			logger.log(Level.INFO, "User wants to create an account");
			EdenAccountInfo account = dialog.getAccountInfo();
			app.getEdenConnection().login(dialog.getLogin(), dialog.getPassword(), interactiveAuth);

			VerificationCodeDialog vDialog = new VerificationCodeDialog(account.getEmail(), app.getEdenConnection(), 4);
			foo = app.showAndWait(vDialog);
			logger.log(Level.INFO, "End with {0} / {1}", vDialog.getCode(), foo);
			if (foo==CloseType.APPLY) {
				logger.log(Level.INFO, "Entered code {0}", vDialog.getCode());
			}
			refresh();
			return true;
		} else if (foo==CloseType.APPLY) {
			logger.log(Level.INFO, "User already has an account");
			writeEdenCredentials(dialog.getLogin(), dialog.getPassword());
//			app.getEdenConnection().getAuthenticator().setInteractiveFallback(interactiveAuth);
			new Thread(() -> {
				logger.log(Level.INFO, "Perform post GUI steps again");
				app.runStartupStepsBlocking(app.getPostGUISteps());
				logger.log(Level.INFO, "done performing post GUI steps again - is application thread="+Platform.isFxApplicationThread());
				// UI is refreshed here @AccountDetailsPage#initInteractivity()
				// from listening to EdenConnection
			}).start();
			return true;
		} else if (foo==CloseType.NO) {
			logger.log(Level.INFO, "User does not want create an account");
			return false;
		} else {
			logger.log(Level.INFO, "User does not want create an account");
			return false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private void writeEdenCredentials(String user, String pass) {
		SettingsService service = Services.get(SettingsService.class).get();
		if (user!=null)
			service.store(PREF_USER, user);
		else
			service.remove(PREF_USER);
		if (pass!=null)
			service.store(PREF_PASS, pass);
		else
			service.remove(PREF_PASS);
	}

	//-------------------------------------------------------------------
	private void showAccountUpdateDialog() {
		AccountDetailsUpdatePane pane = new AccountDetailsUpdatePane();
		pane.setData(app.getAccountInfo());
		CloseType closed =app.showAlertAndCall(AlertType.APPLY_CANCEL, "Change data", pane, pane.getButtonControl());
		logger.log(Level.WARNING, "Closed via "+closed);
		if (closed==CloseType.APPLY) {
			try {
				EdenAccountInfo newData = app.getEdenConnection().updateAccount(pane.getData());
				logger.log(Level.INFO, "Update successful - refresh UI");
				app.setAccountInfo(newData);
				refresh();
			} catch (EdenAPIException e) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, ResourceI18N.format(RES,"section.accountdetails.update.error", e.getMessage()),e);
			}
		}
	}

}
