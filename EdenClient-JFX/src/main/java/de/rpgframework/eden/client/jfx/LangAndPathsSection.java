package de.rpgframework.eden.client.jfx;

import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Section;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.javafx.skin.SectionSkin;
import org.prelle.javafx.skin.SectionSkinNonAcrylic;

import com.gluonhq.attach.device.DeviceService;
import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.storage.StorageService;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Skin;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class LangAndPathsSection extends Section {

	private static ResourceBundle RES = ResourceBundle.getBundle(EdenClientApplication.class.getName(), EdenClientApplication.class.getModule());

	private SettingsService settings;
	private DeviceService device;
	private ChoiceBox<Locale> cbLocale;
	private Label lbLogDir, lbCharDir;

	private Button btnLogDir, btnCharDir;

	private GridPane grid;
	private int nextLine;

	//-------------------------------------------------------------------
	public LangAndPathsSection(Locale...lang) {
		super(ResourceI18N.get(RES, "section.langAndPath"), null);
		initComponents(lang);
		initLayout();
		initInteractivity();
		setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(this, 5);
		FlexGridPane.setMinHeight(this, 4);
		FlexGridPane.setMediumWidth(this, 8);
		//FlexGridPane.setMediumHeight(this, 6);

		refresh();
	}
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
   protected Skin<?> createDefaultSkin() {
        return new SectionSkinNonAcrylic(this);
    }

	//-------------------------------------------------------------------
	private void initComponents(Locale...lang) {
		SettingsService.create().ifPresent( serv -> settings=serv);
		DeviceService.create().ifPresent( serv -> device=serv);

		cbLocale = new ChoiceBox<Locale>();
		cbLocale.getItems().add(null);
		cbLocale.getItems().addAll(lang);
		cbLocale.setConverter(new StringConverter<Locale>() {
			public String toString(Locale loc) {
				return (loc==null)?ResourceI18N.get(RES, "language.auto"):ResourceI18N.get(RES, "language."+loc.getLanguage());
			}
			public Locale fromString(String string) {return null;}
		});

		lbLogDir  = new Label(System.getProperty("logdir"));
		lbCharDir = new Label(System.getProperty("chardir"));

		btnLogDir  = new Button(ResourceI18N.get(RES, "button.open"));
		btnCharDir = new Button(ResourceI18N.get(RES, "button.open"));

		if (device!=null && "Android".equals(device.getPlatform())) {
			btnLogDir.setVisible(false);
			btnCharDir.setVisible(false);

			StorageService.create().ifPresent(service -> {
				String prefix = service.getPublicStorage("").get().toString();
				lbLogDir.setText(System.getProperty("logdir").substring(prefix.length()));
				lbCharDir.setText(System.getProperty("chardir").substring(prefix.length()));
			});
		}
	}

	//-------------------------------------------------------------------
	private Label createLabel(String i18n) {
		return createLabel(i18n, RES);
	}

	//-------------------------------------------------------------------
	private Label createLabel(String i18n, ResourceBundle res) {
		Label label = new Label(ResourceI18N.get(res, i18n));
		label.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		GridPane.setValignment(label, VPos.CENTER);
		return label;
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		nextLine = 0;
		grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(10);
		if (settings!=null) {
			grid.add(createLabel("section.langAndPath.language"), 0, nextLine);
			grid.add(cbLocale, 1, nextLine);
			nextLine++;
			Label lbNewStart = new Label(ResourceI18N.get(RES, "section.langAndPath.requriesRestart"));
			lbNewStart.setStyle("-fx-text-fill: highlight");
			grid.add(lbNewStart, 1, nextLine);
			nextLine++;
		}
		grid.add(createLabel("section.langAndPath.logDir"), 0, nextLine);
		grid.add(btnLogDir, 1, nextLine++);
		grid.add( lbLogDir, 0, nextLine++, 2,1);
		GridPane.setMargin(btnLogDir, new Insets(10, 0, 0, 0));

		grid.add(createLabel("section.langAndPath.charDir"), 0, nextLine);
		grid.add(btnCharDir, 1, nextLine++);
		grid.add( lbCharDir, 0, nextLine++, 2,1);
		GridPane.setMargin(btnCharDir, new Insets(10, 0, 0, 0));


		ScrollPane scroll = new ScrollPane(grid);
		scroll.setFitToWidth(true);
		setContent(scroll);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnLogDir.setOnAction(ev -> BabylonEventBus.fireEvent(BabylonEventType.OPEN_FILE, Paths.get(lbLogDir.getText())) );
		btnCharDir.setOnAction(ev -> BabylonEventBus.fireEvent(BabylonEventType.OPEN_FILE, Paths.get(lbCharDir.getText())) );
		cbLocale.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			// What key is used for storing preferred language
			String langKey = System.getProperty("eden.langkey");
			if (n==null) {
				settings.remove(langKey);
			} else {
				settings.store(langKey, n.getLanguage());
			}
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
//		Preferences pref = Preferences.userNodeForPackage(ComLinkMain.class);
//		return Locale.forLanguageTag(pref.get(key, Locale.getDefault().getLanguage()));
		if (settings!=null) {
			// What key is used for storing preferred language
			String langKey = System.getProperty("eden.langkey");
			String lang = (langKey!=null)?settings.retrieve(langKey):null;
			if (lang==null)
				cbLocale.setValue(null);
			else
				cbLocale.setValue(Locale.forLanguageTag(lang));
		}
	}

	//-------------------------------------------------------------------
	public void addOneLine(ResourceBundle res, String i18n, Node element) {
		grid.add(createLabel(i18n,res), 0, nextLine);
		grid.add(element, 1, nextLine);
		GridPane.setMargin(element, new Insets(10, 0, 0, 0));
		nextLine++;
	}

}
