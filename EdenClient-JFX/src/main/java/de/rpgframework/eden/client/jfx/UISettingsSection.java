package de.rpgframework.eden.client.jfx;

import java.util.Locale;
import java.util.ResourceBundle;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Section;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.javafx.skin.SectionSkinNonAcrylic;

import com.gluonhq.attach.settings.SettingsService;

import de.rpgframework.ResourceI18N;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Skin;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class UISettingsSection extends Section {

	private final static String NONTRANS_KEY = "section.nontransparent";

	private static ResourceBundle RES = ResourceBundle.getBundle(EdenClientApplication.class.getName(), EdenClientApplication.class.getModule());

	private SettingsService settings;
	private ToggleSwitch cbNonTransparent;

	private GridPane grid;
	private int nextLine;

	//-------------------------------------------------------------------
	public UISettingsSection() {
		super(ResourceI18N.get(RES, "section.uisettings"), null);
		initComponents();
		initLayout();
		initInteractivity();
		setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(this, 5);
		FlexGridPane.setMinHeight(this, 4);
		FlexGridPane.setMediumWidth(this, 8);
		//FlexGridPane.setMediumHeight(this, 6);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		SettingsService.create().ifPresent( serv -> settings=serv);

		cbNonTransparent = new ToggleSwitch();
	}

	//-------------------------------------------------------------------
	private Label createLabel(String i18n) {
		return createLabel(i18n, RES);
	}

	//-------------------------------------------------------------------
	private Label createLabel(String i18n, ResourceBundle res) {
		Label label = new Label(ResourceI18N.get(res, i18n));
		label.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		GridPane.setValignment(label, VPos.CENTER);
		return label;
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		nextLine = 0;
		grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(10);
		grid.add(createLabel("section.uisettings.nontransparent"), 0, nextLine);
		grid.add(cbNonTransparent, 1, nextLine++);

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setFitToWidth(true);
		setContent(scroll);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbNonTransparent.selectedProperty().addListener( (ov,o,n) -> {
			// What key is used for storing preferred language
			if (n==null) {
				settings.remove(NONTRANS_KEY);
			} else {
				settings.store(NONTRANS_KEY, String.valueOf(n));
				System.setProperty(NONTRANS_KEY, String.valueOf(n));
			}
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		if (settings!=null) {
			String state = settings.retrieve(NONTRANS_KEY);
			boolean useNonTransparent = (state==null)?false:Boolean.parseBoolean(state);
			cbNonTransparent.setSelected(useNonTransparent);
		}
	}

}
