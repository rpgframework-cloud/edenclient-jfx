package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import de.rpgframework.ResourceI18N;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class RecoverDialog extends ManagedDialog {

	private final static ResourceBundle RES = ResourceBundle.getBundle(RecoverDialog.class.getPackageName()+".Dialogs");
	private final static Logger logger = System.getLogger(RecoverDialog.class.getPackageName());

	private TextField tfCode;
	private PasswordField tfPass1, tfPass2;

	//-------------------------------------------------------------------
	public RecoverDialog(String user, String serverMessage) {
		super(ResourceI18N.format(RES,"recover.title", user), null, CloseType.APPLY, CloseType.CANCEL);
		initComponents();
		initLayout();
		initInteractivity();
		buttonDisabledProperty().put(CloseType.APPLY, true);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfCode  = new TextField();
		tfCode.setPrefColumnCount(6);
		tfPass1 = new PasswordField();
		tfPass1.setPrefColumnCount(10);
		tfPass2 = new PasswordField();
		tfPass2.setPrefColumnCount(10);
	}

	//-------------------------------------------------------------------
	private Boolean buttonControlCallback(CloseType close) {
		logger.log(Level.ERROR, "buttonControl("+close+")");
		if (close==CloseType.APPLY) return Boolean.FALSE;
		return Boolean.TRUE;
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbInstruct= new Label(ResourceI18N.get(RES,"recover.instruct"));
		lbInstruct.setWrapText(true);

		Label lbCode= new Label(ResourceI18N.get(RES,"field.code.label"));
		lbCode.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbPass1 = new Label(ResourceI18N.get(RES,"field.pass.label"));
		lbPass1.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label lbPass2 = new Label(ResourceI18N.get(RES,"field.pass2.label"));
		lbPass2.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		VBox layout = new VBox(lbInstruct, lbCode, tfCode, lbPass1, tfPass1, lbPass2, tfPass2);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfCode.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<4) {
				tfCode.getStyleClass().add("invalid-content");
			} else {
				tfCode.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfPass1.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<6) {
				tfPass1.getStyleClass().add("invalid-content");
			} else {
				tfPass1.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
		tfPass2.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()<6) {
				tfPass1.getStyleClass().add("invalid-content");
			} else {
				tfPass1.getStyleClass().remove("invalid-content");
			}
			refreshButtons();
		});
	}

	//-------------------------------------------------------------------
	private boolean isDataValid() {
		String code  = tfCode.getText();
		String pass1 = tfPass1.getText();
		String pass2 = tfPass2.getText();
		if (code==null || code.length()<4 )
			return false;
		if (pass1==null || pass1.length()<6 )
			return false;
		if (pass2==null || pass2.length()<6 )
			return false;
		return pass1.equals(pass2);
	}

	//-------------------------------------------------------------------
	private void refreshButtons() {
		boolean valid = isDataValid();
		buttonDisabledProperty().put(CloseType.APPLY, !valid);
	}

	public String getCode() { return tfCode.getText(); }
	public String getPassword() { return tfPass1.getText(); }

}
