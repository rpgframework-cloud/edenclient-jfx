package de.rpgframework.eden.client.jfx;

import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;

import de.rpgframework.ResourceI18N;
import javafx.scene.Node;

/**
 * @author prelle
 *
 */
public class EdenSettingsPage extends Page {

	public final static ResourceBundle RES = ResourceBundle.getBundle(PDFPage.class.getPackageName()+".EdenPages");

	protected FlexGridPane flex;

	protected LangAndPathsSection secLangPath;
	protected UISettingsSection secUISettings;

	//-------------------------------------------------------------------
	public EdenSettingsPage(Locale ...lang) {
		super(ResourceI18N.get(RES, "page.settings.title"));

		flex = new FlexGridPane();
		flex.setSpacing(20);
		initComponents(lang);
		flex.getChildren().addAll(secLangPath, secUISettings);
		setContent(flex);
	}

	//-------------------------------------------------------------------
	private void initComponents(Locale[] lang) {
		secLangPath = new LangAndPathsSection(lang);
		FlexGridPane.setMinWidth(secLangPath, 5);
		FlexGridPane.setMinHeight(secLangPath, 5);
		FlexGridPane.setMediumWidth(secLangPath, 6);
		FlexGridPane.setMediumHeight(secLangPath, 5);

		secUISettings = new UISettingsSection();
		FlexGridPane.setMinWidth(secUISettings, 5);
		FlexGridPane.setMinHeight(secUISettings, 5);
		FlexGridPane.setMediumWidth(secUISettings, 6);
		FlexGridPane.setMediumHeight(secUISettings, 5);
	}

	//-------------------------------------------------------------------
	public void addSettingsOneLine(ResourceBundle res, String i18n, Node element) {
		secLangPath.addOneLine(res, i18n, element);
	}

	//-------------------------------------------------------------------
	public void addSection(Node node) {
		flex.getChildren().addAll(node);
	}

}
