package de.rpgframework.eden.client.jfx.steps;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.jfx.EdenClientApplication;

/**
 * @author prelle
 *
 */
public class PrepareEdenConnectionStep implements StartupStep, EdenClientConstants {

	private final static int PORT = 8721;

	protected static Logger logger = System	.getLogger(PrepareEdenConnectionStep.class.getPackageName());

	private EdenClientApplication parent;
	private String appName;
	private String EDEN_SERVER;
	private String scheme;

	//-------------------------------------------------------------------
	public PrepareEdenConnectionStep(EdenClientApplication parent, String appName, String edenServer, String scheme) {
		this.parent = parent;
		this.appName= appName;
		this.EDEN_SERVER = edenServer;
		this.scheme = scheme;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			EdenConnection eden = new EdenConnection(scheme, EDEN_SERVER, PORT, appName, System.getProperty("project.version", "NO VERSION INFO"));
			parent.setEdenConnection(eden);
			logger.log(Level.INFO, "Prepared connection {0}", eden);
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	@Override
	public boolean canRun() {
		return true;
	}

}
