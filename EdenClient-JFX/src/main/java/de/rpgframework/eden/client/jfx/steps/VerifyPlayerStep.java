package de.rpgframework.eden.client.jfx.steps;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertManager;
import org.prelle.javafx.CloseType;

import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.util.Services;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.EdenConnection.StateFlag;
import de.rpgframework.eden.client.OnlineStartupStep;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.LoginDialog;
import de.rpgframework.eden.client.jfx.VerificationCodeDialog;
import javafx.application.Platform;

/**
 * @author prelle
 *
 */
public class VerifyPlayerStep implements OnlineStartupStep, EdenClientConstants {

	private final static ResourceBundle RES = ResourceBundle.getBundle(LoginDialog.class.getPackageName()+".Dialogs");

	protected static Logger logger = System	.getLogger(VerifyPlayerStep.class.getPackageName());

	private EdenClientApplication parent;
	private SettingsService service;

	//-------------------------------------------------------------------
	public VerifyPlayerStep(EdenClientApplication parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * Read user credentials from SettingService - if present - or from
	 * Preferences otherwise
	 */
	private String[] readEdenCredentials() {
		String user = service.retrieve(PREF_USER);
		String pass = service.retrieve(PREF_PASS);
		return new String[] {user,pass};
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private void writeEdenCredentials(String user, String pass) {
		SettingsService service = Services.get(SettingsService.class).get();
		service.store(PREF_USER, user);
		service.store(PREF_PASS, pass);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.DEBUG, "ENTER {0}",getClass().getSimpleName());
		try {
			EdenAccountInfo info = parent.getAccountInfo();
			EdenConnection eden = parent.getEdenConnection();
			if (!info.isVerified()) {
				logger.log(Level.INFO, "Account not verified yet - request verifcation from server");
				try {
					eden.requestAccountVerification();
				} catch (EdenAPIException e) {
					logger.log(Level.ERROR, "Failed to log into server ",e);
					String message = ResourceI18N.format(RES, "verification.fail.server", info.getLogin(), e.getMessage());
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, message, e);
					return;
				}

				VerificationCodeDialog dialog = new VerificationCodeDialog(parent.getAccountInfo().getEmail(), eden, 4);
				Platform.runLater( () -> {
					CloseType close = AlertManager.showAlertAndCall(dialog);
					logger.log(Level.INFO, "Closed {0}",close);
					if (close==CloseType.OK || close==CloseType.YES) {
						logger.log(Level.INFO, "Verification successful");
						info.setVerified(true);
						eden.setStateFlag(StateFlag.ACCOUNT_VERIFIED);
						// Update UI
						parent.setAccountInfo(info);
					} else {
						logger.log(Level.INFO, "Verification failed");
						info.setVerified(false);
						// Update UI
						parent.setAccountInfo(info);
					}
				});
			} else {
				logger.log(Level.INFO, "Account is verified");
				eden.setStateFlag(StateFlag.ACCOUNT_VERIFIED);
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		if (parent.isOffline()) {
			logger.log(Level.INFO, "No verification attempt, since server could not be contacted");
			return false;
		}

		service = Services.get(SettingsService.class).orElse(null);
		if (service==null) {
			logger.log(Level.ERROR, "No SettingService available");
			return false;
		}
		String login = readEdenCredentials()[0];
		if (login==null) {
			logger.log(Level.WARNING, "No login configured");
		}
		return parent.getEdenConnection()!=null && parent.getEdenConnection().hasStateFlag(StateFlag.CREDENTIALS_CORRECT);
	}

}
