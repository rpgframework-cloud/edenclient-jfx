package de.rpgframework.eden.client.jfx;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.TitledComponent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.api.EdenAccountInfo;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

/**
 *
 */
public class AccountDetailsUpdatePane extends GridPane {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AccountDetailsUpdatePane.class.getPackageName()+".Panes");

	private TextField tfEmail;
	private TextField tfFirst, tfLast;
	private PasswordField tfPass1;
	private PasswordField tfPass2;
	private ChoiceBox<Locale> cbLoc;

	private NavigButtonControl btnControl;

	//-------------------------------------------------------------------
	/**
	 */
	public AccountDetailsUpdatePane() {
		btnControl = new NavigButtonControl();
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfPass1 = new PasswordField();
		tfPass2 = new PasswordField();

		tfEmail = new TextField();
		tfEmail.setPromptText(ResourceI18N.get(RES,"field.email.prompt"));

		tfFirst = new TextField();
		tfFirst.setPromptText(ResourceI18N.get(RES,"field.first.prompt"));

		tfLast = new TextField();
		tfLast.setPromptText(ResourceI18N.get(RES,"field.last.prompt"));

		cbLoc = new ChoiceBox<>();
		cbLoc.getItems().addAll(Locale.ENGLISH, Locale.GERMAN, Locale.FRENCH, Locale.forLanguageTag("pt"), Locale.forLanguageTag("ru"), Locale.forLanguageTag("jp"));
		if (System.getProperty("user.language")!=null) {
			cbLoc.getSelectionModel().select(Locale.forLanguageTag(System.getProperty("user.language")));
		}

		cbLoc.setConverter(new StringConverter<Locale>() {
			public String toString(Locale loc) {return (loc!=null)?loc.getDisplayName():"-";}
			public Locale fromString(String string) {return Locale.getDefault();}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setVgap(5);
		setHgap(10);
		add(new TitledComponent(ResourceI18N.get(RES,"field.pass.label"), tfPass1).setTitleMinWidth(150d), 0,0);
		add(new TitledComponent(ResourceI18N.get(RES,"field.pass2.label"), tfPass2).setTitleMinWidth(150d), 1,0);
		add(new TitledComponent(ResourceI18N.get(RES,"field.email.label"), tfEmail).setTitleMinWidth(150d), 0,1);
		add(new TitledComponent(ResourceI18N.get(RES,"field.lang.label"), cbLoc).setTitleMinWidth(150d), 1,1);
		add(new TitledComponent(ResourceI18N.get(RES,"field.first.label"), tfFirst).setTitleMinWidth(150d), 0,3);
		add(new TitledComponent(ResourceI18N.get(RES,"field.last.label"), tfLast).setTitleMinWidth(150d), 1,3);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfPass1.textProperty().addListener((obs, oldVal, newVal) -> checkButtons());
		tfPass2.textProperty().addListener((obs, oldVal, newVal) -> checkButtons());
		tfEmail.textProperty().addListener((obs, oldVal, newVal) -> checkButtons());
	}

	//-------------------------------------------------------------------
	private void checkButtons() {
		String email = tfEmail.getText();
		if (email==null || email.isEmpty() || !email.contains("@") || email.length()<5) {
			btnControl.setDisabled(CloseType.APPLY,true);
			return;
		}
		String pass1 = tfPass1.getText();
		String pass2 = tfPass2.getText();
		if (pass1==null && pass2!=null) { btnControl.setDisabled(CloseType.APPLY,true); return; }
		if (pass2==null && pass1!=null) { btnControl.setDisabled(CloseType.APPLY,true); return; }
		if (pass1!=null && pass2!=null && !pass1.equals(pass2)) {
			btnControl.setDisabled(CloseType.APPLY,true);
			return;
		}
		btnControl.setDisabled(CloseType.APPLY,false);
	}

	//-------------------------------------------------------------------
	public void setData(EdenAccountInfo info) {
		if (info==null) return;

		tfEmail.setText(info.getEmail());
		tfFirst.setText(info.getFirstName());
		tfLast.setText(info.getLastName());
		cbLoc.setValue(info.getLocale());
	}

	//-------------------------------------------------------------------
	public EdenAccountInfo getData() {
		EdenAccountInfo info = new EdenAccountInfo()
				.setEmail(tfEmail.getText())
				.setFirstName(tfFirst.getText())
				.setLastName(tfLast.getText())
				.setLanguage(cbLoc.getValue().getLanguage());
		return info;
	}

	//-------------------------------------------------------------------
	public void clear() {
		tfEmail.clear();
		tfPass1.setText(null);
		tfPass2.setText(null);
		tfFirst.clear();
		tfLast.clear();
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

}
