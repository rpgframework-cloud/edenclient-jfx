package de.rpgframework.eden.client.jfx;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.TitledComponent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.api.EdenAccountInfo;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

/**
 *
 */
public class AccountDetailsPane extends GridPane {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AccountDetailsPane.class.getPackageName()+".Panes");

	private Label tfUser;
	private Label tfEmail;
	private Label tfFirst, tfLast;
	private Label cbLoc;

	//-------------------------------------------------------------------
	/**
	 */
	public AccountDetailsPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfUser = new Label();
		tfEmail = new Label();
		tfFirst = new Label();
		tfLast = new Label();
		cbLoc = new Label();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setVgap(5);
		setHgap(10);
		add(new TitledComponent(ResourceI18N.get(RES,"field.user.label"), tfUser).setTitleMinWidth(150d), 0,0);
		add(new TitledComponent("", new Label()).setTitleMinWidth(100d), 1,0);
		add(new TitledComponent(ResourceI18N.get(RES,"field.email.label"), tfEmail).setTitleMinWidth(150d), 0,1);
		add(new TitledComponent(ResourceI18N.get(RES,"field.lang.label"), cbLoc).setTitleMinWidth(100d), 1,1);
		add(new TitledComponent(ResourceI18N.get(RES,"field.first.label"), tfFirst).setTitleMinWidth(150d), 0,3);
		add(new TitledComponent(ResourceI18N.get(RES,"field.last.label"), tfLast).setTitleMinWidth(100d), 1,3);
	}

	//-------------------------------------------------------------------
	public void setData(EdenAccountInfo info) {
		if (info==null) return;

		tfEmail.setText(info.getEmail());
		tfFirst.setText(info.getFirstName());
		tfLast.setText(info.getLastName());
		tfUser.setText(info.getLogin());
		cbLoc.setText(info.getLocale().getDisplayName());

	}

	//-------------------------------------------------------------------
	public void clear() {
		tfEmail.setText(null);
		tfUser.setText(null);
		tfFirst.setText(null);
		tfLast.setText(null);
		cbLoc.setText(null);
	}

}
