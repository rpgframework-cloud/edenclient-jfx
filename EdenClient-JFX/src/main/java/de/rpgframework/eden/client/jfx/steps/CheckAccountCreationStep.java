package de.rpgframework.eden.client.jfx.steps;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.javafx.CloseType;

import com.gluonhq.attach.settings.SettingsService;
import com.gluonhq.attach.util.Services;

import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.client.EdenClientConstants;
import de.rpgframework.eden.client.jfx.AccountCreationDialog;
import de.rpgframework.eden.client.jfx.AskForPasswordAuthenticator;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.VerificationCodeDialog;
import javafx.application.Platform;

/**
 * @author prelle
 *
 */
public class CheckAccountCreationStep implements StartupStep, EdenClientConstants {

	// True, if the user has already been asked to create an account
	public final static String CFG_ALREADY_ASKED_ACCOUNT = "eden.askedAccount";

	protected static Logger logger = System	.getLogger(CheckAccountCreationStep.class.getPackageName());

	private EdenClientApplication parent;
	private SettingsService service;

	private CloseType userAction;

	//-------------------------------------------------------------------
	public CheckAccountCreationStep(EdenClientApplication parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			logger.log(Level.INFO, "CFG_ALREADY_ASKED_ACCOUNT = "+service.retrieve(CFG_ALREADY_ASKED_ACCOUNT));
			boolean needToAsk = (service.retrieve(CFG_ALREADY_ASKED_ACCOUNT)==null) || !Boolean.parseBoolean( service.retrieve(CFG_ALREADY_ASKED_ACCOUNT) );
			if (needToAsk) {
				// No account given and not asked yet
				logger.log(Level.INFO, "User must be asked for account creation");
				askAccountCreation();
				logger.log(Level.INFO, "Done asking");
			} else {
				logger.log(Level.INFO, "User has already been asked for account creation");
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return TRUE when logging in should be tried, FALSE if no online account exists
	 */
	private boolean askAccountCreation() {
		AccountCreationDialog dialog = new AccountCreationDialog(parent.getEdenConnection(), (l,p)->writeEdenCredentials(l,p));
//		CloseType foo = parent.showAndWait(dialog);
		CloseType foo = showDialog(dialog);
		AskForPasswordAuthenticator interactiveAuth = new AskForPasswordAuthenticator(() -> parent.getEdenConnection(), (l,p)->writeEdenCredentials(l,p), parent.getSecurityDialogImage());
		logger.log(Level.INFO, "Thread "+Thread.currentThread()+"  isFX="+Platform.isFxApplicationThread());
		if (foo==CloseType.YES) {
			logger.log(Level.INFO, "User wanted to create an account");
			EdenAccountInfo account = dialog.getAccountInfo();
			parent.getEdenConnection().login(dialog.getLogin(), dialog.getPassword(), interactiveAuth);

			logger.log(Level.INFO, "Now let user enter the verification code");
			VerificationCodeDialog vDialog = new VerificationCodeDialog(account.getEmail(), parent.getEdenConnection(), 4);
			if (Platform.isFxApplicationThread()) {
				foo = parent.showAndWait(vDialog);
				if (foo==CloseType.APPLY) {
					logger.log(Level.INFO, "Entered code {0}", vDialog.getCode());
				}
//			} else {
//				Platform.runLater(() -> {
//					CloseType foo2 = parent.showAndWait(vDialog);
//					if (foo2==CloseType.APPLY) {
//						logger.log(Level.INFO, "Entered code {0}", vDialog.getCode());
//					}
//					parent.close(vDialog, foo2);
//				});
			}
			return true;
		} else if (foo==CloseType.APPLY) {
			logger.log(Level.INFO, "User already has an account");
			if (parent.getEdenConnection()!=null) {
				parent.getEdenConnection().getAuthenticator().setInteractiveFallback(interactiveAuth);
			}
			writeEdenCredentials(dialog.getLogin(), dialog.getPassword());
			service.store(CFG_ALREADY_ASKED_ACCOUNT, "true");
			return true;
		} else if (foo==CloseType.NO) {
			service.store(CFG_ALREADY_ASKED_ACCOUNT, "true");
			logger.log(Level.INFO, "User does not want create an account");
			return false;
		} else {
			logger.log(Level.INFO, "User does not want create an account");
			return false;
		}
	}

	//-------------------------------------------------------------------
	private CloseType showDialog(AccountCreationDialog dialog) {
		synchronized (dialog) {
		Platform.runLater( () -> {
			userAction = parent.showAndWait(dialog);
			synchronized (dialog) {
				dialog.notify();
			}
		});
			try {
				logger.log(Level.WARNING, "Waiting started in "+Thread.currentThread());
				dialog.wait();
				logger.log(Level.WARNING, "Waiting ended with "+userAction);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		return userAction;
	}

	//-------------------------------------------------------------------
	/**
	 * Write user credentials to SettingService - if present - or ro
	 * Preferences otherwise
	 */
	private void writeEdenCredentials(String user, String pass) {
		SettingsService service = Services.get(SettingsService.class).get();
		if (user!=null)
			service.store(PREF_USER, user);
		else
			service.remove(PREF_USER);

		if (pass!=null)
			service.store(PREF_PASS, pass);
		else
			service.remove(PREF_PASS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		service = Services.get(SettingsService.class).orElse(null);
		if (service==null) {
			logger.log(Level.ERROR, "No SettingService available");
			return false;
		}
		if (parent.isOffline()) {
			logger.log(Level.WARNING, "Server is offline - don't offer account creation");
			return false;
		}
		return true;
	}

}
