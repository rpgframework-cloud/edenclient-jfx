package de.rpgframework.eden.client.jfx.steps;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.jfx.EdenClientApplication;

/**
 * @author prelle
 *
 */
public class CheckForUpdatesStep implements StartupStep {

	protected static Logger logger = System	.getLogger(CheckForUpdatesStep.class.getPackageName());

	private EdenClientApplication parent;
	private String appName;

	//-------------------------------------------------------------------
	public CheckForUpdatesStep(EdenClientApplication parent, String appName) {
		this.parent  = parent;
		this.appName = appName;
	}

	//-------------------------------------------------------------------
	private static boolean isNewerVersionAvailable(String installed, String available) {
		if (available==null)
			return false;

		if (installed.equals(available)) {
			logger.log(Level.INFO,"Version is up to date");
			return false;
		}

		StringTokenizer tokInst = new StringTokenizer(installed, ". -");
		StringTokenizer tokAvai = new StringTokenizer(available, ". -");

		try {
			for (int i=0; i<3; i++) {
				if (!tokInst.hasMoreTokens() || !tokAvai.hasMoreTokens())
					break;
				Integer valI = Integer.parseInt(tokInst.nextToken());
				Integer valA = Integer.parseInt(tokAvai.nextToken());
				if (valA>valI) {
					logger.log(Level.INFO,"'"+available+"' is newer than '"+installed+"'");
					return true;
				} else if (valA<valI) {
					logger.log(Level.INFO,"'"+available+"' is older than '"+installed+"'");
					return false;
				}
			}
		} catch (NumberFormatException e) {
			logger.log(Level.WARNING,"Failed parsing versions: installed="+installed+"  remote="+available+"  : "+e);
			return false;
		} catch (NoSuchElementException e) {
			logger.log(Level.WARNING,"Failed parsing versions: installed="+installed+"  remote="+available+"  : "+e);
			return true;
		}

		logger.log(Level.WARNING,"Assuming that '"+available+"' is newer than '"+installed+"'");
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		String installed = System.getProperty("project.version", "99.99.99");
		try {
			URL updateURL = new URL("http://updates.rpgframework.de/"+appName.toLowerCase()+"-updates/version.txt");
			logger.log(Level.INFO, "Update check at "+updateURL);
			HttpClient client = HttpClient.newBuilder().build();
			HttpRequest request = HttpRequest.newBuilder(updateURL.toURI())
					.setHeader("User-Agent", appName+" "+installed)
					.GET()
					.build();
			HttpResponse<Stream<String>> resp = client.send(request, BodyHandlers.ofLines());
			if (resp.statusCode()==200) {
//			HttpURLConnection con = (HttpURLConnection) updateURL.openConnection();
//			con.setRequestProperty("User-Agent", String.format("%s / %s", installed, System.getProperty("os.name")));
//			int code = con.getResponseCode();
//			if (code==200) {
//				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.defaultCharset()));
//				Stream<String> stream = in.lines();
				Stream<String> stream = resp.body();
				Iterator<String> lines = stream.collect(Collectors.toList()).iterator();
				String version = lines.next();
				String dateRaw = lines.next();
				String changelogURL = lines.hasNext()?lines.next():null;
				String downloadURL = lines.hasNext()?lines.next():"https://www.rpgframework.de/commlink6/download";

				boolean newerAvailable = isNewerVersionAvailable(installed, version);
				if (newerAvailable) {
					LocalDate date = LocalDate.parse(dateRaw,new DateTimeFormatterBuilder()
						    .appendPattern("yyyy-MM-dd")
						    .parseDefaulting(ChronoField.NANO_OF_DAY, 0)
						    .toFormatter()
						    .withZone(ZoneId.of("Europe/Berlin")));
					//(new SimpleDateFormat("YYYY-MM-DD")).parse(date)
					parent.showUpdateMessage(installed, version, date, changelogURL, downloadURL);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return true;
	}

}
