package de.rpgframework.eden.client.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.reality.BoughtItem;
import javafx.application.Platform;

/**
 *
 */
public class AccountDetailsPage extends Page {

	private final static ResourceBundle RES = ResourceBundle.getBundle(AccountDetailsPage.class.getPackageName()+".EdenPages");

	protected static Logger logger = System	.getLogger(AccountDetailsPage.class.getPackageName());

	private AccountDetailsSection secDetails;
	private BoughtItemsSection secBought;

	private transient EdenClientApplication app;
	private transient RoleplayingSystem rules;

	//-------------------------------------------------------------------
	public AccountDetailsPage(EdenClientApplication app, RoleplayingSystem rules) {
		super(ResourceI18N.get(RES, "page.accountdetails.title"));
		this.app = app;
		this.rules = rules;
		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		initAccountDetails();
		initBoughtItems();
	}

	//-------------------------------------------------------------------
	private void initAccountDetails() {
		secDetails = new AccountDetailsSection(app);
		secDetails.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secDetails, 4);
		FlexGridPane.setMinHeight(secDetails, 5);
		FlexGridPane.setMediumWidth(secDetails, 8);
		FlexGridPane.setMediumHeight(secDetails, 4);
	}

	//-------------------------------------------------------------------
	private void initBoughtItems() {
		secBought = new BoughtItemsSection(app,rules);
		FlexGridPane.setMinWidth(secBought, 4);
		FlexGridPane.setMinHeight(secBought, 6);
		FlexGridPane.setMediumWidth(secBought, 7);
		FlexGridPane.setMediumHeight(secBought, 8);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		FlexGridPane flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secDetails, secBought);

		setContent(flex);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		app.getEdenConnection().addListener( (list) -> {
			logger.log(Level.DEBUG, "refreshing account details "+list);
			if (Platform.isFxApplicationThread()) {
				refresh();
			} else {
				Platform.runLater(this::refresh);
			}
		});

	}

	//-------------------------------------------------------------------
	public void refresh() {
		EdenAccountInfo info = app.getAccountInfo();
		if (info==null) return;

		secDetails.refresh();

		List<BoughtItem> list = app.getEdenConnection().getContentPacks(rules);
		secBought.setData(list);
	}

}
