package de.rpgframework.eden.client.jfx;

import java.util.ResourceBundle;

import org.prelle.javafx.Page;
import org.prelle.javafx.PagePile;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.jfx.pages.DatasetsPage;

/**
 *
 */
public class AccountPage extends PagePile {

	public final static ResourceBundle RES = ResourceBundle.getBundle(PDFPage.class.getPackageName()+".EdenPages");

	private AccountDetailsPage pgDetails;
	private DatasetsPage pgDatasets;

	//-------------------------------------------------------------------
	public AccountPage(EdenClientApplication app, RoleplayingSystem rules, Page...pages) {
		super(ResourceI18N.get(RES, "page.account.title"));
		setId("account-page");

		pgDetails = new AccountDetailsPage(app, rules);
		pgDatasets = new DatasetsPage();
		getPages().addAll(pgDetails);
		for (Page page : pages)
			getPages().add(page);
		getPages().addAll(pgDatasets);
	}

}
